import * as io from 'socket.io-client';
import { URL_BACKOFFICE, URL_SOCKET } from '../../shared/host.constant';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable({
    providedIn: 'root'
})

export class ChatService {
    private url = URL_SOCKET;
    private socket;

    constructor() {
        this.socket = io.connect(this.url);
    }


    public sendMessage(username, message) {
        console.log('messge running');
        this.socket.emit('new_message', { username, message });
    }

    public getMessages = () => {
        return Observable.create((observer) => {
            this.socket.on('new_message', (message) => {
                observer.next(message);
            });
        });
    }

    public getMessagesExternal = () => {
        return Observable.create((observer) => {
            this.socket.on('External_message', (message) => {
                observer.next(message);
            });
        });
    }
   
    public getMessagesInternal = () => {
        return Observable.create((observer) => {
            this.socket.on('Internal_message', (message) => {
                observer.next(message);
            });
        });
    }
}
