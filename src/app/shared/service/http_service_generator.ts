import { Injectable } from '@angular/core';
// import { RuntimeEnvironment } from '../env';
import { Http } from '@angular/http';
import { HttpApi } from './http_service';
import { URL_BACKOFFICE, URL_INTERNAL } from '../host.constant';

@Injectable()
export class HttpServiceGenerator {
    constructor(
        // private env: RuntimeEnvironment,
        private http: Http
    ) {
    }

    private get host() {
        var ip = window.location.origin;
        var subIp = ip.substring(0, (ip.length - 4));

        return URL_BACKOFFICE;
        // return this.env.Platform.Http;
    }

    make<T>(uri = '') {
        if (!uri.startsWith("/")) {
            uri = "/" + uri;
        }
        return new HttpApi<T>(`${this.host}${uri}`, this.http);
    }

    private get hostInternal() {
        var ip = window.location.origin;
        var subIp = ip.substring(0, (ip.length - 4));

        return URL_INTERNAL;
        // return this.env.Platform.Http;
    }

    makeInternal<T>(uri = '') {
        if (!uri.startsWith("/")) {
            uri = "/" + uri;
        }
        return new HttpApi<T>(`${this.hostInternal}${uri}`, this.http);
    }
}
