import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckRoleService {
  private  roles: [];
  private isRoleUser = false;
  private isRoleAdmin = false;
  private isAccessCaptcha = false;

  constructor() {
    this.roles = JSON.parse(localStorage.getItem('Role'));
  }

  public checkRoleAccount() {
    if (this.roles) {
      for (const role of this.roles) {
        if (role == 1) {
          this.isRoleUser = true;
        } else if (role == 2) {
          this.isRoleAdmin = true;
        } else if (role == 3) {
          this.isAccessCaptcha = true;
        }
      }
    }
    return {
      isRoleUser: this.isRoleUser,
      isRoleAdmin: this.isRoleAdmin,
      isAccessCaptcha: this.isAccessCaptcha
    };
  }
}
