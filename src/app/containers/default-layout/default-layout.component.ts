import { Component, OnInit } from '@angular/core';
import { navItems } from '../../_nav';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from '../../breadcrumb/breadcrumb.service';
import { ChatService } from '../../shared/service/chat.service';
import { FormGroup, FormControl } from '@angular/forms';
import {CheckRoleService} from '../../shared/service/check-role.service';

const MINUTES_UNITL_AUTO_LOGOUT = 60;
const MAX_TIME_SINGUP = 1440;
const CHECK_INTERVAL = 15000;
const STORE_KEY = 'lastAction';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent implements OnInit {

  constructor(private _router: Router,
    private route: ActivatedRoute,
    private breadCrumbService: BreadcrumbService,
    private chatService: ChatService,
    private checkRole: CheckRoleService
  ) {
    this.currentUserName = localStorage.getItem('NameUser');
    this.checkRoleAccount = this.checkRole.checkRoleAccount();
  }
  public sidebarMinimized = false;
  public navItems = navItems;
  isCustomerShow = true;
  isInternalShow = true;
  isSystemShow = true;
  isCaptchaShow = true;
  isMonitorShow =  true;

  name: string;
  menu: Array<any> = [];
  breadcrumbList: Array<any> = [];
  currentUserName: string;
  checkRoleAccount: { isAccessCaptcha: boolean; isRoleAdmin: boolean; isRoleUser: boolean };
  countNotify = 0;
  countNotifyMessage = 0;
  countNotifyAPI = 0;
  formMess = new FormGroup({
    message: new FormControl('')
  });
  messages: string[] = [];
  dataExternal: string[] = [];
  dataInternal: string[] = [];
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }


  ngOnInit(): void {
    this.menu = this.breadCrumbService.getMenu();
    this.listenRouting();
    this.redirectPage();
    this.check();
    this.initListener();
    this.initInterval();
    this.chatService
      .getMessages()
      .subscribe((message: string) => {
        this.countNotify++;
        this.countNotifyMessage++;
        this.messages.push(message);
      });

    this.chatService
      .getMessagesExternal()
      .subscribe((message: string) => {
        this.countNotify++;
        this.countNotifyAPI++;
        this.dataExternal.push(message);
      });

    this.chatService
      .getMessagesInternal()
      .subscribe((message: string) => {
        this.countNotify++;
        this.countNotifyAPI++;
        this.dataInternal.push(message);
      });

    localStorage.setItem(STORE_KEY, Date.now().toString());
  }

  sendMessage() {
    if (this.formMess.value.message) {
      this.chatService.sendMessage(this.currentUserName, this.formMess.value.message);
      this.formMess.reset('');
    }
  }

  public getLastAction() {
    return parseInt(localStorage.getItem('lastAction'));
  }

  public getLoginTime() {
    return parseInt(localStorage.getItem('loginTime'));
  }

  public setLastAction(lastAction: number) {
    localStorage.setItem(STORE_KEY, lastAction.toString());
  }

  initListener() {
    document.body.addEventListener('click', () => this.reset());
    document.body.addEventListener('mouseover', () => this.reset());
    document.body.addEventListener('mouseout', () => this.reset());
    document.body.addEventListener('keydown', () => this.reset());
    document.body.addEventListener('keyup', () => this.reset());
    document.body.addEventListener('keypress', () => this.reset());
  }

  reset() {
    this.setLastAction(Date.now());
  }

  initInterval() {
    setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);
  }

  check() {
    if (this.getLoginTime() && this.getLastAction()) {
      const now = Date.now();
      const timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
      const realTimeLogin = this.getLoginTime() + MAX_TIME_SINGUP * 60 * 1000;
      const diff = timeleft - now;
      const tokenTime = realTimeLogin - now;
      const isTimeout = diff < 0;
      const isTimeoutToken = tokenTime < 0;
      if (isTimeoutToken) {
        localStorage.clear();
        document.getElementById('openModalNotifyLogout').click();
      }
      if (isTimeout) {
        localStorage.clear();
        document.getElementById('openModalNotifyLogout').click();
      }
    }
  }

  redirectPage() {
    if (this.checkRoleAccount.isAccessCaptcha) {
      this._router.navigateByUrl('/monitor/chart');
    } else {
      this._router.navigateByUrl('/monitor/chart');
    }
  }

  logout(closeConfirmLogout: HTMLButtonElement) {
    localStorage.clear();
    closeConfirmLogout.click();
    location.reload();
  }

  customerShow() {
    if (this.isCustomerShow) {
      this.isCustomerShow = false;
    } else {
      this.isCustomerShow = true;
    }
  }
  internalShow() {
    if (this.isInternalShow) {
      this.isInternalShow = false;
    } else {
      this.isInternalShow = true;
    }
  }
  systemShow() {
    if (this.isSystemShow) {
      this.isSystemShow = false;
    } else {
      this.isSystemShow = true;
    }
  }
  captchaShow() {
    if (this.isCaptchaShow) {
      this.isCaptchaShow = false;
    } else {
      this.isCaptchaShow = true;
    }
  }

  monitorShow() {
    if (this.isMonitorShow) {
      this.isMonitorShow = false;
    } else {
      this.isMonitorShow = true;
    }
  }

  listenRouting() {
    let routerUrl: string, routerList: Array<any>, target: any;
    this._router.events.subscribe((router: any) => {
      routerUrl = router.urlAfterRedirects;
      if (routerUrl && typeof routerUrl === 'string') {
        target = this.menu;
        this.breadcrumbList.length = 0;
        routerList = routerUrl.slice(1).split('/');
        routerList.forEach((router, index) => {
          target = target.find(page => page.path.slice(2) === router);
          this.breadcrumbList.push({
            name: target.name,
            path: (index === 0) ? target.path : `${this.breadcrumbList[index - 1].path}/${target.path.slice(2)}`
          });
          if (index + 1 !== routerList.length) {
            target = target.children;
          }
        });

        console.log(this.breadcrumbList);
      }
    });
  }

  closeNotifyAndLogout(closeModalNotifyLogout: HTMLButtonElement) {
    location.reload();
  }

  notiClickList() {
    this.countNotifyAPI = 0;
    this.countNotify = this.countNotifyMessage;
  }

  notiClickMessage() {
    this.countNotifyMessage = 0;
    this.countNotify = this.countNotifyAPI;
  }

  redirectToChart() {
    this._router.navigateByUrl('/monitor/chart');
  }
}
