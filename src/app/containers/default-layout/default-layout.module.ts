import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, HashLocationStrategy, DatePipe } from '@angular/common';
import { DefaultLayoutComponent } from './default-layout.component';
import { ManagementCustomerComponent } from '../management-customers/management-customer/management-customer.component';
import { ManagementContractComponent } from '../management-customers/management-contract/management-contract.component';
import { ManagementCodeComponent } from '../management-internal/management-code/management-code.component';
import { ManagementCicRpViewComponent } from '../management-internal/management-cic-rp-view/management-cic-rp-view.component';
import { ManagementUserComponent } from '../management-system/management-user/management-user.component';
import { ManagementCaptchaAdminComponent } from '../management-system/management-captcha-admin/management-captcha-admin.component';
import { ManualCaptchaComponent } from '../manual-captcha-b002/manual-captcha.component';
import { ManualCaptchaB003Component } from '../manual-captcha-b003/manual-captcha-b003.component';
import { ManualCaptchaB001Component } from '../manual-captcha-b001/manual-captcha-b001.component';
import { AppAsideModule, AppBreadcrumbModule, AppFooterModule, AppHeaderModule, AppSidebarModule } from '@coreui/angular';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BasicAuthInterceptor } from '../../shared/service/Interceptor';
import { AuthProvider, SessionValidationGuard } from '../../auth';
import { HttpServiceGenerator } from '../../shared';
import { RouterModule } from '@angular/router';
import {MatInputModule} from '@angular/material/input';
import {AdminCanActive} from '../../auth/shared/AdminCanActive';
import {UserCanActive} from '../../auth/shared/UserCanActive';
import {CaptchaUserCanActive} from '../../auth/shared/CaptchaUserCanActive';


@NgModule({
  declarations: [
    DefaultLayoutComponent,
    ManagementCustomerComponent,
    ManagementContractComponent,
    ManagementCodeComponent,
    ManagementCicRpViewComponent,
    ManagementUserComponent,
    ManagementCaptchaAdminComponent,
    ManualCaptchaComponent,
    ManualCaptchaB003Component,
    ManualCaptchaB001Component
  ],
  imports: [
    CommonModule,
    RouterModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppSidebarModule,
    MatInputModule,
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,

    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthInterceptor,
      multi: true
    },
    AuthProvider,
    HttpServiceGenerator,
    SessionValidationGuard,
    AdminCanActive,
    UserCanActive,
    CaptchaUserCanActive,
    DatePipe
  ]
})
export class DefaultLayoutModule { }
