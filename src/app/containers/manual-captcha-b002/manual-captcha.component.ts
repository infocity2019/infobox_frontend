import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {CICReportService} from '../management-internal/management-cic-rp-view/shared/c-i-c-report.service';
import {IFormCICRequest} from '../management-internal/management-cic-rp-view/shared/iform-cicrequest';
import {DatePipe} from '@angular/common';
import {IFormCICResponse} from '../management-internal/management-cic-rp-view/shared/iform-cicresponse';
import {IManualCTRequest} from './shared/IManualCTRequest';
import {log} from 'util';
import {ManualCtService} from './shared/manual-ct.service';
import {ImanualCaptchaRes} from './shared/imanual-captcha-res';
import {IFormCICResponsePagination} from '../management-internal/management-cic-rp-view/shared/IFormCICResponsePagination';
import {PagerService} from '../../shared/service/pager.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-manual-captcha',
  templateUrl: './manual-captcha.component.html',
  styleUrls: ['./manual-captcha.component.css']
})
export class ManualCaptchaComponent implements OnInit {
  formSearchCIC = new FormGroup({
    niceSsID: new FormControl(''),
  });
  formPost = new FormGroup({
    appCd: new FormControl('infotechDev'),
  });
  isLooking: boolean;
  currentLocation = 0;
  limitRow = 10;
  mesQuery: string;
  timeQuery: string;
  listB0002: IFormCICResponsePagination;
  hiddenNext: boolean;
  hiddenPre: boolean;
  CICRp: IFormCICResponse;
  cmtID: string;
  captchaInput: string;
  resCaptcha: ImanualCaptchaRes;
  isSubmitting: boolean;
  countTimeCaptchaEnter = 0;
  messCaptcha: string;
  nameCus: string;
  cicNo: string;
  pagerB0002: any = {};
  niceSsIDPagination: any;
  processCaptcha: Subscription;
  messCaptchaInput: string;
  constructor(private cicReportService: CICReportService,
              private manualCtService: ManualCtService,
              private datePipe: DatePipe,
              private pagerService: PagerService) { }

  ngOnInit(): void {
  }

  closeProcessCaptcha(closeModalB0002: HTMLButtonElement, closeModalConfirm: HTMLButtonElement) {
    if (this.processCaptcha) {
      this.processCaptcha.unsubscribe();
    }
    this.searchB0002();
    closeModalB0002.click();
    closeModalConfirm.click();
  }

  getImageCaptcha() {
    this.messCaptcha = '';
    this.messCaptchaInput = '';
    if (this.CICRp) {
        if (this.CICRp.NATL_ID) {
          this.cmtID = this.CICRp.NATL_ID;
        } else if (this.CICRp.OLD_NATL_ID) {
          this.cmtID = this.CICRp.OLD_NATL_ID;
        } else if (this.CICRp.PSPT_NO) {
          this.cmtID = this.CICRp.PSPT_NO;
        } else {
          console.log('cmt null');
          return;
        }
        if (!this.captchaInput && this.resCaptcha) {
          return this.messCaptchaInput = 'Captcha can not blank!';
        }
        this.isSubmitting = true;
        const form: IManualCTRequest = {
          appCd: 'INFOBOX',
          iftUrl: 'http://www.infotech3.co.kr/ift/deploy/',
          orgCd: 'cic.vn',
          svcCd: 'B0002',
          dispNm: 'cic.org.vn',
          cicNo: '',
          taxNo: '',
          userId: this.CICRp.LOGIN_ID,
          userPw: atob(this.CICRp.LOGIN_PW).substr(14),
          customerType: '2',
          cmtNo: this.cmtID,
          step_useYn: 'Y',
          step_input: this.captchaInput ? this.captchaInput : '',
          step_data: this.resCaptcha ? this.resCaptcha.dataStep : '',
          reportType: '06',
          voteNo: '111',
          niceSessionKey: this.CICRp.NICE_SSIN_ID,
        };
        console.log(form);
        this.processCaptcha = this.manualCtService.getCaptchaImage(form).subscribe(
          result => {
            this.isSubmitting = false;
            if (result.imgBase64) {
              this.countTimeCaptchaEnter += 1;
              this.resCaptcha = result;
              this.captchaInput = '';
            } else if (result.outJson.errMsg) {
              this.messCaptcha = result.outJson.errMsg;
            } else if (result.outJson.outB0002.errMsg) {
              this.messCaptcha = result.outJson.outB0002.errMsg;
            } else if (result.outJson.outB0002.cicNo || result.outJson.outB0002.name) {
              this.resCaptcha = null;
              this.cicNo = result.outJson.outB0002.cicNo;
              this.nameCus = result.outJson.outB0002.name;
            }
          }, error => {
            this.isSubmitting = false;
            this.messCaptcha = error.message;
          }
        );
      }
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }


  setPageSearchB0002(page: number) {
    if (page < 1 || page > this.pagerB0002.totalPages) {
      return;
    }
    if (this.listB0002) {
      // get pager object from service
      this.pagerB0002 = this.pagerService.getPager(this.listB0002.count[0].TOTAL, page);
    }
  }

  searchB0002() {
    this.isLooking = true;
    const {
      niceSsID
    } = this.formSearchCIC.value;

    const form: IFormCICRequest = {
      niceSsID,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(form);
    this.manualCtService.getListDataForManualCTB0002(form).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.niceSsIDPagination = niceSsID;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.listB0002 = result;
          this.setPageSearchB0002(1);
        } else {
          this.mesQuery = 'Not Found!';
          this.getDate();
          this.listB0002 = null ;
        }
      }, error => {
        this.isLooking = false;
      }
    );
  }

  paginationB0002(location: number) {
    const form: IFormCICRequest = {
      niceSsID: this.niceSsIDPagination,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(form);
    this.manualCtService.getListDataForManualCTB0002(form).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.listB0002 = result;
        } else {
          this.listB0002 = null ;
        }
      }
    );
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, CICRp: IFormCICResponse) {
    openModalUpdate.click();
    this.captchaInput = '';
    this.messCaptcha = '';
    this.CICRp = CICRp;
    this.resCaptcha = null;
    this.countTimeCaptchaEnter = 0;
    this.cicNo = null;
    this.nameCus = null;
    this.isSubmitting = false;
    this.messCaptchaInput = '';
  }

  checkToCloseModalB0002(closeModalB0002: HTMLButtonElement, openModalConfirmClose: HTMLButtonElement) {
    if (this.isSubmitting || this.resCaptcha) {
      openModalConfirmClose.click();
    } else {
      closeModalB0002.click();
    }
  }
}
