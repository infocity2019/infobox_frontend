export interface ImanualCaptchaRes {
  imgBase64?: any;
  dataStep?: {
    cookie?: {
      CICORGVN?: any;
      LB_cookies?: any;
    },
    step?: any;
    ViewState?: any;
    ref?: any;
    state?: any;
    afrLoop?: any;
  }
  outJson?: {
    in?: {
      dispNm?: any;
      voteNo?: any;
      appCd?: any;
      svcCd?: any;
      userId?: any;
      cmtNo?: any;
      reportType?: any;
      niceSessionKey?: any;
      customerType?: any;
      thread_id?: any;
      orgCd?: any;
      iftUrl?: any;
      userPw?: any;
      step_data?: {
        cookie?: {
          CICORGVN?: any;
          LB_cookies?: any;
        };
        step?: any;
        ViewState?: any;
        ref?: any;
        state?: any;
        afrLoop?: any;
        cicNo?: any;
        name?: any;
      };
      cicNo?: any;
      step_useYn?: any;
      taxNo?: any;
      step_input?: any;
      engVer?: any;
    };
    errMsg?: any;
    appCd?: any;
    svcCd?: any;
    resMsg?: any;
    outA0001?: {
      errYn?: any;
      errMsg?: any;
    };
    outB0002?: {
      errYn?: any;
      errMsg?: any;
      cicNo?: any;
      name?: any;
    };
    uid?: any;
    outB0003?: {
      errYn?: any;
      errMsg?: any;
    };
    outB0001?: {
      errYn?: any;
      errMsg?: any;
      list?: any;
    };
    orgCd?: any;
    resCd?: any;
    errYn?: any;
  }
}
