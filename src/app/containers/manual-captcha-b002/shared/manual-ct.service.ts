import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../shared/service';
import {IManualCTRequest} from './IManualCTRequest';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ImanualCaptchaRes} from './imanual-captcha-res';
import {IFormCICRequest} from '../../management-internal/management-cic-rp-view/shared/iform-cicrequest';
import {IFormCICResponse} from '../../management-internal/management-cic-rp-view/shared/iform-cicresponse';
import {IFormCICResponsePagination} from '../../management-internal/management-cic-rp-view/shared/IFormCICResponsePagination';

@Injectable({
  providedIn: 'root'
})
export class ManualCtService {
  private internalAPI = this.httpG.makeInternal('/internal');
  private backofficeAPI = this.httpG.make('/manualCaptcha');

  constructor(private httpG: HttpServiceGenerator) {
  }

  getCaptchaImage(form: IManualCTRequest): Observable<ImanualCaptchaRes> {
    return this.internalAPI.Post<ImanualCaptchaRes>('cic', {}, form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  getListDataForManualCTB0002(form: IFormCICRequest): Observable<IFormCICResponsePagination> {
    return this.backofficeAPI.Get<IFormCICResponsePagination>('getListB002', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
