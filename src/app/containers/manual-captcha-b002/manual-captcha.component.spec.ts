import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualCaptchaComponent } from './manual-captcha.component';

describe('ManualCaptchaComponent', () => {
  let component: ManualCaptchaComponent;
  let fixture: ComponentFixture<ManualCaptchaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualCaptchaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualCaptchaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
