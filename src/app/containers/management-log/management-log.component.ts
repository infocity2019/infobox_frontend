import { Component, OnInit } from '@angular/core';
import {Color, Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {FormControl, FormGroup} from '@angular/forms';
import {RequestForm} from './shared/request-form';
import {ChartService} from './shared/log.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-management-log',
  templateUrl: './management-log.component.html',
  styleUrls: ['./management-log.component.scss']
})
export class ManagementLogComponent implements OnInit {
  constructor(private logService: ChartService,
              private datePipe: DatePipe) { }
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.log.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Completed(10)', '00', '01', '02' , '03', '04', '20', '21', '22', '23', '24', '29'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: [
        'rgba(66, 238, 23, 1)',
        'rgba(96, 134, 9, 0.89)',
        'rgba(9, 119, 134, 0.89)',
        'rgba(9, 65, 134, 0.89)',
        'rgba(255, 181, 61, 1)',
        'rgba(246, 72, 4, 1)',
        'rgba(190, 40, 210, 1)',
        'rgba(210, 40, 119, 1)',
        'rgba(200, 0, 28, 0.96)',
        'rgba(64, 38, 41, 1)',
        'rgba(81, 109, 112, 1)',
        'rgba(53, 74, 18, 0.95)',
      ],
    },
  ];
  //
  // bar
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins;
  formUpdateChart = new FormGroup({
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });
  listDay: string[] = [];
  list_10: number[] = [];
  list_00: number[] = [];
  list_01: number[] = [];
  list_02: number[] = [];
  list_03: number[] = [];
  list_04: number[] = [];
  list_20: number[] = [];
  list_21: number[] = [];
  list_22: number[] = [];
  list_23: number[] = [];
  list_24: number[] = [];
  list_29: number[] = [];
  list_ToTal: number[] = [];
  barChartData: ChartDataSets[] = [];
  barChartLabels: Label[] = [];
  notifyChart: string;
  //
  // log line
  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  lineChartColors = [
    {borderColor: 'yellow' , backgroundColor: 'rgba(39, 211, 208, 1)'},
    {borderColor: 'darkgreen' , backgroundColor: 'rgba(66, 238, 23, 1)'},
    {borderColor: 'rgba(96, 134, 9, 0.89)' , backgroundColor: 'rgba(96, 104, 9, 0.89)'},
    {borderColor: 'rgba(70, 119, 134, 0.89)' , backgroundColor: 'rgba(9, 119, 134, 0.89)'},
    {borderColor: 'rgba(50, 65, 134, 0.89)' , backgroundColor: 'rgba(9, 65, 134, 0.89)'},
    {borderColor: 'rgba(200, 181, 61, 1)' , backgroundColor: 'rgba(255, 181, 61, 1)'},
    {borderColor: 'rgba(170, 72, 4, 1)', backgroundColor: 'rgba(246, 72, 4, 1)'},
    {borderColor: 'mediumpurple', backgroundColor: 'rgba(190, 40, 210, 1)'},
    {borderColor: 'deeppink' , backgroundColor: 'rgba(210, 40, 119, 1)'},
    {borderColor: 'darkred', backgroundColor: 'rgba(200, 0, 28, 0.96)'},
    {borderColor: 'rgba(80, 38, 41, 1)', backgroundColor: 'rgba(64, 38, 41, 1)'},
    {borderColor: 'rgba(81, 80, 112, 1)', backgroundColor: 'rgba(81, 109, 112, 1)'},
    {borderColor: 'rgba(40, 74, 18, 0.95)', backgroundColor: 'rgba(53, 74, 18, 0.95)'}
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];
  //
  isChartPie = false;
  isWeek = false;
  isChartLine = false;
  isChartBar = true;
  isRefreshing: boolean;
  sortByDate = new FormControl('month');
  typeChart = new FormControl('bar');

  ngOnInit() {
  }
  // events
  logClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  logHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    this.barChartData[0].data = data;
  }

  async updateChart(modalNotifyChart: HTMLButtonElement) {
    let {fromDate, toDate} = this.formUpdateChart.value;
    const sortBy = this.sortByDate.value;
    const type = this.typeChart.value;
    if (fromDate === '' && toDate === '') {
      return;
    } else if (fromDate === '' && toDate) {
      fromDate = toDate;
    } else if (toDate === '' && fromDate) {
      toDate = fromDate;
    }
    if (type === 'pie') {
      toDate = fromDate;
    }
    this.listDay = [];
    this.list_00 = [];
    this.list_01 = [];
    this.list_02 = [];
    this.list_03 = [];
    this.list_04 = [];
    this.list_10 = [];
    this.list_20 = [];
    this.list_21 = [];
    this.list_22 = [];
    this.list_23 = [];
    this.list_24 = [];
    this.list_29 = [];
    this.list_ToTal = [];
    this.isRefreshing = true;
    const form: RequestForm = {
      toDate: this.datePipe.transform(toDate , 'yyyy,MM,dd'),
      fromDate: this.datePipe.transform(fromDate , 'yyyy,MM,dd')
    };
    console.log(form);
    if (sortBy === 'day') {
    }
    if (sortBy === 'month') {
    }
    if (sortBy === 'year') {
    }
    if (sortBy === 'week') {
    }
  }

  changeTypeChart() {
    const type = this.typeChart.value;
    if (type === 'bar') {
      this.isChartBar = true;
      this.isChartLine = false;
      this.isChartPie = false;
    } else if (type === 'line') {
      this.isChartBar = false;
      this.isChartLine = true;
      this.isChartPie = false;
    } else if (type === 'pie') {
      this.isChartBar = false;
      this.isChartLine = false;
      this.isChartPie = true;
    }
  }

  checkTypeDate() {
    const typeDate = this.sortByDate.value;
    console.log(typeDate);
    if (typeDate === 'week') {
      this.isWeek = true;
    } else {
      this.isWeek = false;
    }
  }
}
