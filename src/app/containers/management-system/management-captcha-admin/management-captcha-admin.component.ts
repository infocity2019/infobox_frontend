import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {IFormRequestUser} from '../management-user/shared/iform-request-user';
import {IFormCaptchaRequest} from './shared/iform-captcha-request';
import {CaptchaService} from './shared/captcha.service';
import {ManageUserService} from '../management-user/shared/manage-user.service';
import {DatePipe} from '@angular/common';
import {IFormCaptchaResponse} from './shared/iform-captcha-response';
import {IFormCaptchaResponsePagination} from './shared/IFormCaptchaResponsePagination';
import {PagerService} from '../../../shared/service/pager.service';
import {IResultCheckTrigger} from './shared/IResultCheckTrigger';
import * as _ from 'lodash';

@Component({
  selector: 'app-management-captcha-admin',
  templateUrl: './management-captcha-admin.component.html',
  styleUrls: ['./management-captcha-admin.component.scss']
})
export class ManagementCaptchaAdminComponent implements OnInit {
  formSearchCaptcha = new FormGroup({
    userID: new FormControl(''),
    userName: new FormControl('')
  });
  formCreateCaptcha = new FormGroup({
    userID: new FormControl(''),
    userName: new FormControl(''),
    phone: new FormControl(''),
    email: new FormControl(''),
    address: new FormControl(''),
    validStartDT: new FormControl(''),
    validEndDT: new FormControl(''),
    finalOperaDate: new FormControl(''),
    workID: new FormControl(''),
  });
  formUpdateCaptcha = new FormGroup({
    CAPTCHA_USER_ID: new FormControl(''),
    CAPTCHA_USER_NM: new FormControl(''),
    ADDR: new FormControl(''),
    EMAIL: new FormControl(''),
    TEL_NO_MOBILE: new FormControl(''),
    SYS_DTIM: new FormControl(''),
    VALID_START_DT: new FormControl(''),
    VALID_END_DT: new FormControl(''),
    WORK_ID: new FormControl(''),
  });
  regexEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  regexNumber = /^[0-9]+$/;
  currentLocation = 0;
  limitRow = 10;
  mesQuery: string;
  timeQuery: string;
  listCaptcha: IFormCaptchaResponsePagination;
  hiddenNext: boolean;
  messageValidCreate: string;
  hiddenPre: boolean;
  ICaptcha: IFormCaptchaResponse;
  messageValidUpdate: string;
  isLooking: boolean;
  isWaitingRegister: boolean;
  isWaitingUpdate: boolean;
  pagerCaptcha: any = {};
  userIDCaptchaPagination: any;
  userNameCaptchaPagination: any;
  status: IResultCheckTrigger;
  checkStatus: boolean;
  isCheckStatus: boolean;
  timeNow: string;
  currentUserName: string;
  constructor(private captchaService: CaptchaService,
              private datePipe: DatePipe,
              private pagerService: PagerService) {
    this.currentUserName = localStorage.getItem('UserName');
  }

  ngOnInit() {
    this.showStatusTrigger();
  }

  setTimeSys() {
    this.timeNow = this.datePipe.transform(new Date(), 'yyyy/MM/dd-hh:mm:ss');
  }

  onSubmit() {
    console.log(this.checkStatus);
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, captcha: IFormCaptchaResponse) {
    this.ICaptcha = captcha;
    this.messageValidUpdate = '';
    openModalUpdate.click();
    this.formUpdateCaptcha.patchValue({
      CAPTCHA_USER_ID: captcha.CAPTCHA_USER_ID,
      CAPTCHA_USER_NM: captcha.CAPTCHA_USER_NM,
      ADDR: captcha.ADDR,
      EMAIL: captcha.EMAIL,
      TEL_NO_MOBILE: captcha.TEL_NO_MOBILE,
      SYS_DTIM: captcha.SYS_DTIM,
      VALID_START_DT:captcha.VALID_START_DT,
      VALID_END_DT: captcha.VALID_END_DT,
      WORK_ID: captcha.WORK_ID,
    });
  }

  registerCaptcha() {
    this.messageValidCreate = '';
    const {
      userID,
      userName,
      phone,
      email,
      address,
      validStartDT,
      validEndDT,
      finalOperaDate,
      workID,
    } = this.formCreateCaptcha.value;

    if (userName === '' || userName === null) {
      this.messageValidCreate = 'User Name require not blank !';
      return;
    }

    if (userID === '' || userID === null) {
      this.messageValidCreate = 'User ID require not blank !';
      return;
    }

    if (validStartDT && validStartDT.length > 10) {
      this.messageValidCreate = 'Valid Start Date wrong!';
      return;
    }

    if (validEndDT && validEndDT.length > 10) {
      this.messageValidCreate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(validStartDT, 'yyyy/MM/dd') > this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid Start Date wrong , Valid Start Date can not bigger than Today!';
      return;
    }

    if (this.datePipe.transform(validEndDT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }

    if (email && !this.regexEmail.test(email)) {
      this.messageValidCreate = 'Email not valid!';
      return;
    }

    if (phone && !this.regexNumber.test(phone)) {
      this.messageValidCreate = 'Phone number not valid!';
      return;
    }
    this.isWaitingRegister = true;
    const form: IFormCaptchaRequest = {
      userID,
      userName,
      phone,
      email,
      address,
      validStartDT: this.datePipe.transform(validStartDT, 'yyyy/MM/dd'),
      validEndDT: this.datePipe.transform(validEndDT, 'yyyy/MM/dd'),
      finalOperaDate: this.timeNow,
      workID: this.currentUserName
    };
    console.log(form);
    this.captchaService.createCaptcha(form).subscribe(
      result => {
        this.isWaitingRegister = false;
        if (result.rowsAffected) {
          this.messageValidCreate = 'Created Captcha';
          this.formCreateCaptcha.reset('');
        }
        if (result.error) {
          this.messageValidCreate = 'User ID has been used.';
        }
      }
    );
  }

  updateCaptcha() {
    const {
      CAPTCHA_USER_ID,
      CAPTCHA_USER_NM,
      ADDR,
      EMAIL,
      TEL_NO_MOBILE,
      SYS_DTIM,
      VALID_START_DT,
      VALID_END_DT,
      WORK_ID,
    } = this.formUpdateCaptcha.value;
    this.messageValidUpdate = '';

    if (_.isEqual(this.formUpdateCaptcha.value, this.ICaptcha)) {
      this.messageValidUpdate = 'Nothing Change!';
      return;
    }

    if (CAPTCHA_USER_NM === '' || CAPTCHA_USER_NM === null) {
      this.messageValidUpdate = 'User Name require not blank !';
      return;
    }

    if (VALID_START_DT && VALID_START_DT.length > 10) {
      this.messageValidUpdate = 'Valid Start Date wrong!';
      return;
    }

    if (VALID_END_DT && VALID_END_DT.length > 10) {
      this.messageValidUpdate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd') > this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidUpdate = 'Valid Start Date wrong , Valid Start Date can not bigger Today!';
      return;
    }

    if (this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidUpdate = 'Valid End Date wrong , Valid End Date must bigger Today!';
      return;
    }

    if (EMAIL && !this.regexEmail.test(EMAIL)) {
      this.messageValidUpdate = 'Email invalid!';
      return;
    }

    if (TEL_NO_MOBILE && !this.regexNumber.test(TEL_NO_MOBILE)) {
      this.messageValidUpdate = 'Phone number invalid!';
      return;
    }
    this.isWaitingUpdate = true;
    const form: IFormCaptchaRequest = {
      userID: CAPTCHA_USER_ID,
      userName: CAPTCHA_USER_NM,
      phone: TEL_NO_MOBILE,
      email: EMAIL,
      address: ADDR,
      validStartDT: this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd'),
      validEndDT: this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd'),
    };
    console.log(form);
    this.captchaService.updateCaptcha(form).subscribe(
      result => {
        this.isWaitingUpdate = false;
        if (result.rowsAffected == 1) {
          this.messageValidUpdate = 'Updated Captcha';
          this.ICaptcha = this.formUpdateCaptcha.value;
          this.searchCaptcha();
        }
      }, error => {
        this.isWaitingUpdate = false;
        this.messageValidUpdate = error.message;
      }
    );
  }

  setPageSearchCaptchaInfo(page: number) {
    if (page < 1 || page > this.pagerCaptcha.totalPages) {
      return;
    }
    if (this.listCaptcha) {
      // get pager object from service
      this.pagerCaptcha = this.pagerService.getPager(this.listCaptcha.count[0].TOTAL, page);
    }
  }

  searchCaptcha() {
    this.isLooking = true;
    const {userID, userName} = this.formSearchCaptcha.value;
    const searchForm: IFormCaptchaRequest = {
      userID,
      userName,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.captchaService.getCaptchaInfo(searchForm).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.userIDCaptchaPagination = userID;
          this.userNameCaptchaPagination = userName;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.listCaptcha = result;
          this.setPageSearchCaptchaInfo(1);
        } else {
          this.listCaptcha = null;
          this.mesQuery = 'Not Found!';
          this.getDate();
        }
      }, error => {
        console.log(error);
      }
    );
  }

  paginationCaptcha(location: number) {
    const searchForm: IFormCaptchaRequest = {
      userID: this.userIDCaptchaPagination,
      userName: this.userNameCaptchaPagination,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.captchaService.getCaptchaInfo(searchForm).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listCaptcha = result;
        } else {
          this.listCaptcha = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

   resetMessValidCreate() {
    this.messageValidCreate = '';
    this.setTimeSys();
  }

  showStatusTrigger() {
    this.captchaService.getStatusTrigger().subscribe(
      result => {
        this.status = result[0];
        if (this.status.STATUS === 'DISABLED') {
          this.checkStatus = false;
        } else if (this.status.STATUS === 'ENABLED') {
          this.checkStatus = true;
        }
      }
    );
  }

  triggerEnable(closeModalEnableTrigger: HTMLButtonElement) {
    this.captchaService.enableTrigger().subscribe(
      result => {
        if (result.rowsAffected == '0') {
          closeModalEnableTrigger.click();
          this.showStatusTrigger();
        }
      }
    );
  }

  triggerDisable(closeModalDisableeTrigger: HTMLButtonElement) {
    this.captchaService.disableTrigger().subscribe(
      result => {
        if (result.rowsAffected == '0') {
          closeModalDisableeTrigger.click();
          this.showStatusTrigger();
        }
      }
    );
  }

  reEnableTrigger() {
    document.getElementById('notChecked').click();
  }

  reDisableTrigger() {
    document.getElementById('checked').click();
  }
}
