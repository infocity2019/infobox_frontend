export interface IFormCaptchaRequest {
  userID?: string;
  userName?: string;
  validStartDT?: string;
  validEndDT?: string;
  phone?: string;
  address?: string;
  email?: string;
  finalOperaDate?: string;
  workID?: string;
  currentLocation?: any;
  limitRow?: any;
}
