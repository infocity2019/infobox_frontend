export interface IFormCaptchaResponse {
  CAPTCHA_USER_ID?: string;
  CAPTCHA_USER_NM?: string;
  ADDR?: string;
  EMAIL?: string;
  TEL_NO_MOBILE?: string;
  SYS_DTIM?: string;
  VALID_START_DT?: string;
  VALID_END_DT?: string;
  WORK_ID?: string;
}
