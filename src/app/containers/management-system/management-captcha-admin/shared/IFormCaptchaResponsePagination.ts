import {IFormCaptchaResponse} from './iform-captcha-response';

export interface IFormCaptchaResponsePagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IFormCaptchaResponse
  ];
}
