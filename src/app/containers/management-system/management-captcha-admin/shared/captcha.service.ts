import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {IFormCaptchaRequest} from './iform-captcha-request';
import {Observable} from 'rxjs';
import {IFormCaptchaResponse} from './iform-captcha-response';
import {tap} from 'rxjs/operators';
import {IFormCaptchaResponsePagination} from './IFormCaptchaResponsePagination';
import {IResultCheckTrigger} from './IResultCheckTrigger';

@Injectable({
  providedIn: 'root'
})
export class CaptchaService {
  private captchaAPI = this.httpG.make('/captcha');
  constructor(private httpG: HttpServiceGenerator) { }

  getCaptchaInfo(form: IFormCaptchaRequest): Observable<IFormCaptchaResponsePagination> {
    return this.captchaAPI.Get<IFormCaptchaResponsePagination>('getCaptchaInfo', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  createCaptcha(form: IFormCaptchaRequest): Observable<any> {
    return this.captchaAPI.Post<any>('createCaptcha', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  updateCaptcha(form: IFormCaptchaRequest): Observable<any> {
    return this.captchaAPI.Put<any>('updateCaptcha', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  getStatusTrigger(): Observable<any> {
    return this.captchaAPI.Get<any>('getTriggerStatus').pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  enableTrigger(): Observable<any> {
    return this.captchaAPI.Put<any>('enableTrigger').pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  disableTrigger(): Observable<any> {
    return this.captchaAPI.Put<any>('disableTrigger').pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
