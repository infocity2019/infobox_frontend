import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCaptchaAdminComponent } from './management-captcha-admin.component';

describe('ManagementCaptchaAdminComponent', () => {
  let component: ManagementCaptchaAdminComponent;
  let fixture: ComponentFixture<ManagementCaptchaAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementCaptchaAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCaptchaAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
