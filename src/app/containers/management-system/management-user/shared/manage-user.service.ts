import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {IFormRequestUser} from './iform-request-user';
import {Observable} from 'rxjs';
import {IFormResponseUser} from './iform-response-user';
import {tap} from 'rxjs/operators';
import {IFormResponseUserPagination} from './IFormResponseUserPagination';

@Injectable({
  providedIn: 'root'
})
export class ManageUserService {
  private userAPI = this.httpG.make('/user');
  constructor(private httpG: HttpServiceGenerator) { }

  getUserInfo(form: IFormRequestUser): Observable<IFormResponseUserPagination> {
    return this.userAPI.Post<IFormResponseUserPagination>('getUserInfo', {} ,form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  getRoleAccount(userID: {}): Observable<any> {
    return this.userAPI.Get<any>('getRoleAccount' , userID).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  createUser(form: IFormRequestUser): Observable<any> {
    return this.userAPI.Post<any>('createUser', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  updateUser(form: IFormRequestUser): Observable<any> {
    return this.userAPI.Put<any>('updateUser', {}, form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  resetPW(form: IFormRequestUser): Observable<any> {
    return this.userAPI.Put<any>('resetPassword', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
