export interface IFormRequestUser {
  userID?: string;
  userClass?: string;
  userName?: string;
  orgCode?: string;
  validStartDT?: string;
  validEndDT?: string;
  phone?: string;
  address?: string;
  email?: string;
  finalOperaDate?: string;
  workID?: string;
  userPW?: string;
  currentLocation?: any;
  limitRow?: any;
  active?: number[];
  role?: number[];
}
