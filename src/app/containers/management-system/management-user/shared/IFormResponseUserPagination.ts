import {IFormResponseUser} from './iform-response-user';

export interface IFormResponseUserPagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IFormResponseUser
  ];
}
