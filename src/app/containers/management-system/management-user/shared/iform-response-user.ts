export interface IFormResponseUser {
  ACTIVE?: number;
  ROLE?: string;
  ROLE_ID?: number[];
  USER_ID?: string;
  USER_NM?: string;
  CUST_CD?: string;
  CUST_NM?: string;
  INOUT_GB?: string;
  ADDR?: string;
  EMAIL?: string;
  TEL_NO_MOBILE?: string;
  SYS_DTIM?: string;
  VALID_START_DT?: string;
  VALID_END_DT?: string;
  WORK_ID?: string;
}
