import { Component, OnInit } from '@angular/core';
import {ManageUserService} from './shared/manage-user.service';
import {FormControl, FormGroup} from '@angular/forms';
import {IFormSearchCode} from '../../management-internal/management-code/shared/iform-search-code';
import {DatePipe} from '@angular/common';
import {IFormRequestUser} from './shared/iform-request-user';
import {IFormResponseUser} from './shared/iform-response-user';
import {IFormSearchCust} from '../../management-customers/management-customer/shared/IFormSearchCust';
import {ManageCustomerService} from '../../management-customers/management-customer/shared/manageCustomer.service';
import {ICustInfo} from '../../management-customers/management-customer/shared/ICustInfo';
import {IcustInfoArray} from '../../management-customers/management-customer/shared/IcustInfoArray';
import {PagerService} from '../../../shared/service/pager.service';
import {IFormResponseUserPagination} from './shared/IFormResponseUserPagination';
import * as _ from 'lodash';
import {CheckRoleService} from '../../../shared/service/check-role.service';

@Component({
  selector: 'app-management-user',
  templateUrl: './management-user.component.html',
  styleUrls: ['./management-user.component.scss']
})
export class ManagementUserComponent implements OnInit {
  formSearchUser = new FormGroup({
    userID: new FormControl(''),
    userName: new FormControl(''),
    userClass: new FormControl(''),
    active: new FormControl(''),
    orgCode: new FormControl('')
  });
  formSearchOrgModal = new FormGroup({
    custClassicfication: new FormControl(''),
    cusCd: new FormControl(''),
    custNm: new FormControl(''),
  });
  formCreateUser = new FormGroup({
    userID: new FormControl(''),
    userClass: new FormControl(''),
    userName: new FormControl(''),
    orgCode: new FormControl(''),
    orgName: new FormControl(''),
    validStartDT: new FormControl(''),
    validEndDT: new FormControl(''),
    phone: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl(''),
    finalOperaDate: new FormControl(''),
    workID: new FormControl(''),
    active: new FormControl(''),
    role: new FormControl(''),
  });


  formUpdateUser = new FormGroup({
    ACTIVE: new FormControl(''),
    ROLE: new FormControl(''),
    ROLE_ID: new FormControl(''),
    USER_ID: new FormControl(''),
    USER_NM: new FormControl(''),
    CUST_CD: new FormControl(''),
    CUST_NM: new FormControl(''),
    INOUT_GB: new FormControl(''),
    ADDR: new FormControl(''),
    EMAIL: new FormControl(''),
    TEL_NO_MOBILE: new FormControl(''),
    SYS_DTIM: new FormControl(''),
    VALID_START_DT: new FormControl(''),
    VALID_END_DT: new FormControl(''),
    WORK_ID: new FormControl('')
  });

  currentLocation = 0;
  limitRow = 10;
  mesQuery: string;
  listUser: IFormResponseUserPagination;
  hiddenNext: boolean;
  timeQuery: string;
  mesSearchOrg: string;
  listOrg: IcustInfoArray;
  organizationName: any;
  messageValidCreate: string;
  hiddenPre: boolean;
  IUser: IFormResponseUser;
  userClass: string;
  regexEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  regexNumber = /^[0-9]+$/;
  messageValidUpdate: string;
  isLooking: boolean;
  isLookingOrg: boolean;
  isWaitingReg: boolean;
  isWaitingUpdate: boolean;
  isWaitingRsPW: boolean;
  custClassicficationModal: any;
  cusCdModal: any;
  custNmModal: any;
  pagerOrg: any = {};
  pagerUser: any = {};
  userIDPagination: any;
  userNamePagination: any;
  userClassPagination: any;
  orgCodePagination: any;
  currentUserName: string;
  timeNow: string;
  activeCodePagination: any;
  currentUserID: string;
  currentUserRole: number;
  currentUserJsonStorage: IFormResponseUser;
  checkRoleAcount: { isAccessCaptcha: boolean; isRoleAdmin: boolean; isRoleUser: boolean };
  roleRegister: number[];
  roleUpdate: number[];
  isWaitingGetRole: boolean;
  activeArr: number[] = [];
  constructor(private manageUserService: ManageUserService,
              private datePipe: DatePipe,
              private cusService: ManageCustomerService,
              private checkRole: CheckRoleService,
              private pagerService: PagerService) {
    this.currentUserName = localStorage.getItem('UserName');
    this.currentUserID = localStorage.getItem('UserID');
    this.checkRoleAcount = this.checkRole.checkRoleAccount();
    this.currentUserJsonStorage = JSON.parse(localStorage.getItem('currentUserJson'));
  }

  ngOnInit() {
    console.log(this.currentUserJsonStorage);
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, user: IFormResponseUser) {
    openModalUpdate.click();
    this.isWaitingGetRole = true;
    this.manageUserService.getRoleAccount({userID : user.USER_ID}).subscribe(
      result => {
        if (result[0]) {
          this.isWaitingGetRole = false;
          for (let role of result) {
            if (role.ROLE_ID == 1) {
              this.formUpdateUser.patchValue({
              ROLE : 'User'
              });
            } else if (role.ROLE_ID == 2) {
              this.formUpdateUser.patchValue({
                ROLE : 'Admin'
              });
            } else if (role.ROLE_ID == 3) {
              this.formUpdateUser.patchValue({
                ROLE : 'Captcha'
              });
            }
          }
          this.IUser = user;
          this.messageValidUpdate = '';
          this.IUser.ROLE = this.formUpdateUser.value.ROLE;
          this.IUser.ROLE_ID = null;
          this.formUpdateUser.patchValue({
            ACTIVE: user.ACTIVE,
            ROLE_ID: null,
            USER_ID: user.USER_ID,
            USER_NM: user.USER_NM,
            CUST_CD: user.CUST_CD,
            CUST_NM: user.CUST_NM,
            INOUT_GB: user.INOUT_GB,
            ADDR: user.ADDR,
            EMAIL: user.EMAIL,
            TEL_NO_MOBILE: user.TEL_NO_MOBILE,
            SYS_DTIM: user.SYS_DTIM,
            VALID_START_DT: user.VALID_START_DT,
            VALID_END_DT: user.VALID_END_DT,
            WORK_ID: user.WORK_ID
          });
          console.log(this.formUpdateUser.value);
        }
      }
    );
  }

  createUser() {
    this.messageValidCreate = '';
    const {
      userID,
      userClass,
      userName,
      orgCode,
      validStartDT,
      validEndDT,
      finalOperaDate,
      phone,
      address,
      email,
      workID,
      active,
      role
    } = this.formCreateUser.value;

    if (role == 1) {
      this.roleRegister = [1];
    } else if (role == 2) {
      this.roleRegister = [2];
    } else if (role == 3) {
      this.roleRegister = [3];
    }

    if (userID === '' || userID === null) {
      this.messageValidCreate = 'User ID require not blank !';
      return;
    }

    if (userName === '' || userName === null) {
      this.messageValidCreate = 'User Name require not blank !';
      return;
    }

    if (orgCode === '' || orgCode === null) {
      this.messageValidCreate = 'Organization Code require not blank !';
      return;
    }

    if (userClass === '' || userClass === null) {
      this.messageValidCreate = 'User classification require not blank !';
      return;
    }

    if (validStartDT && validStartDT.length > 10) {
      this.messageValidCreate = 'Valid Start Date wrong!';
      return;
    }

    if (validEndDT && validEndDT.length > 10) {
      this.messageValidCreate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(validStartDT, 'yyyy/MM/dd') > this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid Start Date wrong , Valid Start Date can not bigger than Today!';
      return;
    }

    if (this.datePipe.transform(validEndDT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }

    if (role === '' || role === null) {
      this.messageValidCreate = 'Please choose Role for account!';
      return;
    }

    if (active === '' || active === null) {
      this.messageValidCreate = 'Please set Status for account!';
      return;
    }

    if (email && !this.regexEmail.test(email)) {
      this.messageValidCreate = 'Email not valid!';
      return;
    }

    if (phone && !this.regexNumber.test(phone)) {
      this.messageValidCreate = 'Phone number not valid!';
      return;
    }
    this.isWaitingReg = true;
    const form: IFormRequestUser = {
      userID,
      userClass,
      userName,
      orgCode,
      validStartDT: this.datePipe.transform(validStartDT, 'yyyy/MM/dd'),
      validEndDT: this.datePipe.transform(validEndDT, 'yyyy/MM/dd'),
      phone,
      address,
      email,
      workID: this.currentUserName,
      role: this.roleRegister,
      active
    };
    console.log(form);
    this.manageUserService.createUser(form).subscribe(
      result => {
        this.isWaitingReg = false;
        if (result.message) {
          this.messageValidCreate = result.message;
          this.formCreateUser.reset('');
        }
      }, error => {
        this.isWaitingReg = false;
        this.messageValidCreate = error.message
      }
    );
  }

  updateUser() {
    const {
      ACTIVE,
      ROLE,
      ROLE_ID,
      USER_ID,
      USER_NM,
      CUST_CD,
      CUST_NM,
      INOUT_GB,
      ADDR,
      EMAIL,
      TEL_NO_MOBILE,
      SYS_DTIM,
      VALID_START_DT,
      VALID_END_DT,
      WORK_ID
    } = this.formUpdateUser.value;

    if (ROLE_ID == 1) {
      this.roleUpdate = [1];
      this.formUpdateUser.patchValue({
        ROLE: 'User'
      });
    } else if (ROLE_ID == 2) {
      this.roleUpdate = [2];
      this.formUpdateUser.patchValue({
        ROLE: 'Admin'
      });
    } else if (ROLE_ID == 3) {
      this.roleUpdate = [3];
      this.formUpdateUser.patchValue({
        ROLE: 'Captcha'
      });
    } else {
      this.roleUpdate = [];
    }
    this.messageValidUpdate = '';

    if (_.isEqual(this.formUpdateUser.value, this.IUser)) {
      this.messageValidUpdate = 'Nothing Change!';
      return;
    }

    if (USER_NM === '' || USER_NM === null) {
      this.messageValidUpdate = 'User Name require not blank !';
      return;
    }

    if (CUST_CD === '' || CUST_CD === null) {
      this.messageValidUpdate = 'Organization Code require not blank !';
      return;
    }

    if (INOUT_GB === '' || INOUT_GB === null) {
      this.messageValidUpdate = 'User classification require not blank !';
      return;
    }

    if (VALID_START_DT && VALID_START_DT.length > 10) {
      this.messageValidUpdate = 'Valid Start Date wrong!';
      return;
    }

    if (VALID_END_DT && VALID_END_DT.length > 10) {
      this.messageValidUpdate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd') > this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidUpdate = 'Valid Start Date wrong , Valid Start Date can not bigger than Today!';
      return;
    }

    if (this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd') && ACTIVE == 1) {
      this.messageValidUpdate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }

    if (EMAIL && !this.regexEmail.test(EMAIL)) {
      this.messageValidUpdate = 'Email invalid!';
      return;
    }

    if (TEL_NO_MOBILE && !this.regexNumber.test(TEL_NO_MOBILE)) {
      this.messageValidUpdate = 'Phone number invalid!';
      return;
    }
    this.isWaitingUpdate = true;
    const form: IFormRequestUser = {
      userID: USER_ID,
      userName: USER_NM,
      userClass: INOUT_GB,
      orgCode: CUST_CD,
      validStartDT: this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd'),
      validEndDT: this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd'),
      phone: TEL_NO_MOBILE,
      address: ADDR,
      email: EMAIL,
      workID: WORK_ID,
      role: this.roleUpdate,
      active: ACTIVE
    };
    console.log(form);
    this.manageUserService.updateUser(form).subscribe(
      result => {
        this.isWaitingUpdate = false;
        if (result.rowsAffected == 1) {
          this.messageValidUpdate = 'Updated User';
          this.formUpdateUser.patchValue({
            ROLE_ID: null
          });
          this.IUser = this.formUpdateUser.value;
          this.searchUser();
        }
      }, error => {
        this.isWaitingUpdate = false;
        this.messageValidUpdate = error.message;
      }
    );
  }

  resetPassword(closeModaleRsPW: HTMLButtonElement) {
    this.isWaitingRsPW = true;
    const form: IFormRequestUser = {
      userID: this.IUser.USER_ID
    };
    this.manageUserService.resetPW(form).subscribe(
      result => {
        this.isWaitingRsPW = false;
        if (result.rowsAffected) {
          this.messageValidUpdate = 'Password has been restored.';
          closeModaleRsPW.click();
        }
      }
    );
  }

  searchUser() {
    this.activeArr = [];
    this.isLooking = true;
    let {userID, userName, userClass, orgCode , active} = this.formSearchUser.value;
    if (active == '' || active == 1) {
      this.activeArr.push(1);
    } else if (active == 3) {
      this.activeArr = [];
    } else if (active == 2) {
      this.activeArr.push(0,2);
    }
    const searchForm: IFormRequestUser = {
      userID,
      userName,
      userClass,
      orgCode,
      active: this.activeArr,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.manageUserService.getUserInfo(searchForm).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.userIDPagination = userID;
          this.userNamePagination = userName;
          this.userClassPagination = userClass;
          this.orgCodePagination = orgCode;
          this.activeCodePagination = this.activeArr;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.listUser = result;
          this.setPageSearchUser(1);
        } else {
          this.listUser = null;
          this.mesQuery = 'Not found!';
          this.getDate();
        }
      }, error => {
        this.isLooking = false;
        console.log(error);
      }
    );
  }

  paginationUser(location: number) {
    const searchForm: IFormRequestUser = {
      userID: this.userIDPagination,
      userName: this.userNamePagination,
      userClass: this.userClassPagination,
      orgCode: this.orgCodePagination,
      active: this.activeCodePagination,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.manageUserService.getUserInfo(searchForm).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.listUser = result;
        } else {
          this.listUser = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  setPageSearchCustModal(page: number) {
    if (page < 1 || page > this.pagerOrg.totalPages) {
      return;
    }
    if (this.listOrg) {
      // get pager object from service
      this.pagerOrg = this.pagerService.getPager(this.listOrg.count[0].TOTAL, page);
    }
  }

  setPageSearchUser(page: number) {
    if (page < 1 || page > this.pagerUser.totalPages) {
      return;
    }
    if (this.listUser) {
      // get pager object from service
      this.pagerUser = this.pagerService.getPager(this.listUser.count[0].TOTAL, page);
    }
  }

  searchOrgByNameOrClass() {
    this.isLookingOrg = true;
    const {custClassicfication, cusCd , custNm} = this.formSearchOrgModal.value;
    const form: IFormSearchCust = {
      custClassicfication,
      cusCd,
      custNm,
      status: [1] ,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        this.isLookingOrg = false;
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.custClassicficationModal = custClassicfication;
          this.cusCdModal = cusCd;
          this.custNmModal = custNm;
          this.listOrg = result;
          this.setPageSearchCustModal(1);
        } else {
          this.listOrg = null;
        }
      }, error => {
        this.isLookingOrg = false;
      }
    );
  }

  paginationCustModal(location: number) {
    const form: IFormSearchCust = {
      custClassicfication: this.custClassicficationModal,
      cusCd: this.cusCdModal,
      custNm: this.custNmModal,
      status: [1] ,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listOrg = result;
        } else {
          this.listOrg = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  getDataToInputSearch(closeOrnName: HTMLButtonElement, cust: ICustInfo) {
    this.organizationName = cust.CUST_NM;
    this.formSearchUser.patchValue({
      orgCode: cust.CUST_CD
    });
    closeOrnName.click();
  }

  getDataToInputRegister(closeOrnNameRegister: HTMLButtonElement, cust: ICustInfo) {
    this.formCreateUser.patchValue({
      orgCode: cust.CUST_CD,
      orgName: cust.CUST_NM,
    });
    closeOrnNameRegister.click();
  }

  resetRegisterMes() {
    this.messageValidCreate = '';
    this.timeNow = this.datePipe.transform(new Date(), 'yyyy/MM/dd-hh:mm:ss');
  }

  getDataToInputUpdate(closeOrnNameUpdate: HTMLButtonElement, cust: ICustInfo) {
    this.formUpdateUser.patchValue({
      CUST_CD: cust.CUST_CD,
      CUST_NM: cust.CUST_NM
    });
    closeOrnNameUpdate.click();
  }
}
