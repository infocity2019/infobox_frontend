import { TestBed } from '@angular/core/testing';

import { CountTransactionService } from './count-transaction.service';

describe('CountTransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CountTransactionService = TestBed.get(CountTransactionService);
    expect(service).toBeTruthy();
  });
});
