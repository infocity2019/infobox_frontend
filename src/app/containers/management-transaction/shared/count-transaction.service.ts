import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../shared/service';
import {IFormSearchCust} from '../../management-customers/management-customer/shared/IFormSearchCust';
import {Observable} from 'rxjs';
import {ICustInfo} from '../../management-customers/management-customer/shared/ICustInfo';
import {tap} from 'rxjs/operators';
import {Transaction} from './transaction';

@Injectable({
  providedIn: 'root'
})
export class CountTransactionService {
  private cusApi = this.httpG.make('/countTransactions');
  constructor(private httpG: HttpServiceGenerator) {}

  countTransactions(form: any): Observable<Transaction[]> {
    return this.cusApi.Get<Transaction[]>('count', form).pipe
    (tap(result => {
    }, e => {
    }));
  }
}
