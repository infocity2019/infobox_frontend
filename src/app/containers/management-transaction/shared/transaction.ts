export interface Transaction {
  TOTAL: number;
  COMPLETED: number;
  FAILED: number;
  DATE: string;
}
