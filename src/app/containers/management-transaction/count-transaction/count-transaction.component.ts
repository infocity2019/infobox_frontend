import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {CountTransactionService} from '../shared/count-transaction.service';
import {Transaction} from '../shared/transaction';

@Component({
  selector: 'app-count-transaction',
  templateUrl: './count-transaction.component.html',
  styleUrls: ['./count-transaction.component.scss']
})
export class CountTransactionComponent implements OnInit {
  formDate = new FormGroup({
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });
  status: string;
  dateInquiry: string;
  resultCount: Transaction[] = [];
  isRefreshing: boolean;
  constructor(public datePipe: DatePipe,
              private countTransactionService: CountTransactionService) { }

  ngOnInit() {
  }

  doCountTransaction() {
    let {fromDate, toDate} = this.formDate.value;
    if (fromDate === '' && toDate === '') {
      return;
    } else if (fromDate === '' && toDate) {
      fromDate = toDate;
    } else if (toDate === '' && fromDate) {
      toDate = fromDate;
    }
    this.isRefreshing = true;
    this.status = '';
    this.dateInquiry = '';
    const form = {
      fromDate: this.datePipe.transform(fromDate , 'yyyy,MM,dd'),
      toDate: this.datePipe.transform(toDate , 'yyyy,MM,dd')
    };
    this.countTransactionService.countTransactions(form).subscribe(
      result => {
        this.isRefreshing = false;
        this.getDate();
        this.status = 'Successfully';
        this.resultCount = result;
      }, error => {
        this.isRefreshing = false;
        this.getDate();
        this.status = 'Failed';
        console.log(error);
      }
    );
  }

  getDate() {
    const now = new Date();
    this.dateInquiry = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }
}
