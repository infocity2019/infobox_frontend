import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountTransactionComponent } from './count-transaction.component';

describe('CountTransactionComponent', () => {
  let component: CountTransactionComponent;
  let fixture: ComponentFixture<CountTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
