export interface DataOfChart {
  SMS?: number;
  RP_IQ_RQ_OK?: number;
  CIC_LOGIN_OK?: number;
  CIC_ID_IQ_OK?: number;
  CIC_RP_IQ_OK?: number;
  COMPLETED?: number;
  LOGIN_ERROR?: number;
  CIC_ID_IQ_ERROR?: number;
  CIC_RP_IQ_ERROR?: number;
  CIC_RQ_RS_IQ_ERROR?: number;
  SCRAP_TG_RP_NOT_EXIST?: number;
  OTHER_ERROR?: number;
  TOTAL?: number;
  DATE?: string;
}
