import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../shared/service';
import {RequestForm} from './request-form';
import {Observable} from 'rxjs';
import {DataOfChart} from './data-of-chart';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  private chartAPI = this.httpG.make('/chart');
  constructor(private httpG: HttpServiceGenerator) { }

  getDataChartFromDateToDate(form: RequestForm): Observable<DataOfChart[]> {
    return this.chartAPI.Get<DataOfChart[]>('getDataFromDayToDayChartBar', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
  getDataChartFromMonthToMonth(form: RequestForm): Observable<DataOfChart[]> {
    return this.chartAPI.Get<DataOfChart[]>('getDataMonthFromMonth', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
  getDataChartFromYearToYear(form: RequestForm): Observable<DataOfChart[]> {
    return this.chartAPI.Get<DataOfChart[]>('getDataYear', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
  getDataChartByWeek(form: RequestForm): Observable<DataOfChart[]> {
    return this.chartAPI.Get<DataOfChart[]>('getDataWeek', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
