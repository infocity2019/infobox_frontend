import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementChartComponent } from './management-chart.component';

describe('ManagementChartComponent', () => {
  let component: ManagementChartComponent;
  let fixture: ComponentFixture<ManagementChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
