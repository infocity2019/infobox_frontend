import { Component, OnInit } from '@angular/core';
import {Color, Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {FormControl, FormGroup} from '@angular/forms';
import {RequestForm} from './shared/request-form';
import {ChartService} from './shared/chart.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-management-chart',
  templateUrl: './management-chart.component.html',
  styleUrls: ['./management-chart.component.scss']
})
export class ManagementChartComponent implements OnInit {
  constructor(private chartService: ChartService,
              private datePipe: DatePipe) { }
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Completed(10)', '00', '01', '02' , '03', '04', '20', '21', '22', '23', '24', '29'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: [
        'rgba(66, 238, 23, 1)',
        'rgba(96, 134, 9, 0.89)',
        'rgba(9, 119, 134, 0.89)',
        'rgba(9, 65, 134, 0.89)',
        'rgba(255, 181, 61, 1)',
        'rgba(246, 72, 4, 1)',
        'rgba(190, 40, 210, 1)',
        'rgba(210, 40, 119, 1)',
        'rgba(200, 0, 28, 0.96)',
        'rgba(64, 38, 41, 1)',
        'rgba(81, 109, 112, 1)',
        'rgba(53, 74, 18, 0.95)',
      ],
    },
  ];
  //
  // bar
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins;
  formUpdateChart = new FormGroup({
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });
  listDay: string[] = [];
  list_10: number[] = [];
  list_00: number[] = [];
  list_01: number[] = [];
  list_02: number[] = [];
  list_03: number[] = [];
  list_04: number[] = [];
  list_20: number[] = [];
  list_21: number[] = [];
  list_22: number[] = [];
  list_23: number[] = [];
  list_24: number[] = [];
  list_29: number[] = [];
  list_ToTal: number[] = [];
  barChartData: ChartDataSets[] = [];
  barChartLabels: Label[] = [];
  notifyChart: string;
  //
  // chart line
  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  lineChartColors = [
    {borderColor: 'yellow' , backgroundColor: 'rgba(39, 211, 208, 1)'},
    {borderColor: 'darkgreen' , backgroundColor: 'rgba(66, 238, 23, 1)'},
    {borderColor: 'rgba(96, 134, 9, 0.89)' , backgroundColor: 'rgba(96, 104, 9, 0.89)'},
    {borderColor: 'rgba(70, 119, 134, 0.89)' , backgroundColor: 'rgba(9, 119, 134, 0.89)'},
    {borderColor: 'rgba(50, 65, 134, 0.89)' , backgroundColor: 'rgba(9, 65, 134, 0.89)'},
    {borderColor: 'rgba(200, 181, 61, 1)' , backgroundColor: 'rgba(255, 181, 61, 1)'},
    {borderColor: 'rgba(170, 72, 4, 1)', backgroundColor: 'rgba(246, 72, 4, 1)'},
    {borderColor: 'mediumpurple', backgroundColor: 'rgba(190, 40, 210, 1)'},
    {borderColor: 'deeppink' , backgroundColor: 'rgba(210, 40, 119, 1)'},
    {borderColor: 'darkred', backgroundColor: 'rgba(200, 0, 28, 0.96)'},
    {borderColor: 'rgba(80, 38, 41, 1)', backgroundColor: 'rgba(64, 38, 41, 1)'},
    {borderColor: 'rgba(81, 80, 112, 1)', backgroundColor: 'rgba(81, 109, 112, 1)'},
    {borderColor: 'rgba(40, 74, 18, 0.95)', backgroundColor: 'rgba(53, 74, 18, 0.95)'}
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];
  //
  isChartPie = false;
  isWeek = false;
  isChartLine = false;
  isChartBar = true;
  isRefreshing: boolean;
  sortByDate = new FormControl('month');
  typeChart = new FormControl('bar');

  ngOnInit() {
  }
  // events
  chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    this.barChartData[0].data = data;
  }

  async updateChart(modalNotifyChart: HTMLButtonElement) {
    let {fromDate, toDate} = this.formUpdateChart.value;
    const sortBy = this.sortByDate.value;
    const type = this.typeChart.value;
    if (fromDate === '' && toDate === '') {
      return;
    } else if (fromDate === '' && toDate) {
      fromDate = toDate;
    } else if (toDate === '' && fromDate) {
      toDate = fromDate;
    }
    if (type === 'pie') {
      toDate = fromDate;
    }
    this.listDay = [];
    this.list_00 = [];
    this.list_01 = [];
    this.list_02 = [];
    this.list_03 = [];
    this.list_04 = [];
    this.list_10 = [];
    this.list_20 = [];
    this.list_21 = [];
    this.list_22 = [];
    this.list_23 = [];
    this.list_24 = [];
    this.list_29 = [];
    this.list_ToTal = [];
    this.isRefreshing = true;
    const form: RequestForm = {
      toDate: this.datePipe.transform(toDate , 'yyyy,MM,dd'),
      fromDate: this.datePipe.transform(fromDate , 'yyyy,MM,dd')
    };
    console.log(form);
    if (sortBy === 'day') {
      this.chartService.getDataChartFromDateToDate(form).subscribe(
        async result => {
          if (result[0]) {
            for (const e of result) {
              await this.listDay.push([e.DATE.slice(0, 4), '-', e.DATE.slice(4, 6), '-', e.DATE.slice(6, 8)].join(''));
              await this.list_00.push(e.SMS);
              await this.list_01.push(e.RP_IQ_RQ_OK);
              await this.list_02.push(e.CIC_LOGIN_OK);
              await this.list_03.push(e.CIC_ID_IQ_OK);
              await this.list_04.push(e.CIC_RP_IQ_OK);
              await this.list_10.push(e.COMPLETED);
              await this.list_20.push(e.LOGIN_ERROR);
              await this.list_21.push(e.CIC_ID_IQ_ERROR);
              await this.list_22.push(e.CIC_RP_IQ_ERROR);
              await this.list_23.push(e.CIC_RQ_RS_IQ_ERROR);
              await this.list_24.push(e.SCRAP_TG_RP_NOT_EXIST);
              await this.list_29.push(e.OTHER_ERROR);
              await this.list_ToTal.push(e.TOTAL);
            }
            this.barChartData = [
              {data: this.list_ToTal , label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];
            this.lineChartData = [
              {data: this.list_ToTal, label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];

            this.pieChartData =  [
              this.list_10[0],
              this.list_00[0],
              this.list_01[0],
              this.list_02[0],
              this.list_03[0],
              this.list_04[0],
              this.list_20[0],
              this.list_21[0],
              this.list_22[0],
              this.list_23[0],
              this.list_24[0],
              this.list_29[0]
            ];
            this.isRefreshing = false;
            this.lineChartLabels = this.listDay;
            this.barChartLabels = this.listDay;
            console.log('ok');
          } else {
          }
        }, error => {
          if (error.status === 501) {
            this.isRefreshing = false;
            this.notifyChart = 'Maximum number of displayed days no more than 31 days';
            modalNotifyChart.click();
          } else {
            this.isRefreshing = false;
            this.notifyChart = error.message;
            modalNotifyChart.click();
          }
        }
      );
    }
    if (sortBy === 'month') {
      this.chartService.getDataChartFromMonthToMonth(form).subscribe(
        result => {
          if (result[0]) {
            for (const e of result) {
              this.listDay.push([e.DATE.slice(0, 4), '-', e.DATE.slice(4, 6)].join(''));
              this.list_00.push(e.SMS);
              this.list_01.push(e.RP_IQ_RQ_OK);
              this.list_02.push(e.CIC_LOGIN_OK);
              this.list_03.push(e.CIC_ID_IQ_OK);
              this.list_04.push(e.CIC_RP_IQ_OK);
              this.list_10.push(e.COMPLETED);
              this.list_20.push(e.LOGIN_ERROR);
              this.list_21.push(e.CIC_ID_IQ_ERROR);
              this.list_22.push(e.CIC_RP_IQ_ERROR);
              this.list_23.push(e.CIC_RQ_RS_IQ_ERROR);
              this.list_24.push(e.SCRAP_TG_RP_NOT_EXIST);
              this.list_29.push(e.OTHER_ERROR);
              this.list_ToTal.push(e.TOTAL);
            }
            console.log(this.list_ToTal, this.list_10);
            this.barChartData = [
              {data: this.list_ToTal , label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];
            this.lineChartData = [
              {data: this.list_ToTal, label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];

            this.pieChartData =  [
              this.list_10[0],
              this.list_00[0],
              this.list_01[0],
              this.list_02[0],
              this.list_03[0],
              this.list_04[0],
              this.list_20[0],
              this.list_21[0],
              this.list_22[0],
              this.list_23[0],
              this.list_24[0],
              this.list_29[0]
            ];
            this.isRefreshing = false;
            this.lineChartLabels = this.listDay;
            this.barChartLabels = this.listDay;
            console.log('ok');
          } else {

          }
        }, error => {
          this.isRefreshing = false;
          this.notifyChart = error.message;
          modalNotifyChart.click();
        }
      );
    }
    if (sortBy === 'year') {
      this.chartService.getDataChartFromYearToYear(form).subscribe(
        result => {
          if (result[0]) {
            for (const e of result) {
              this.listDay.push(e.DATE);
              this.list_00.push(e.SMS);
              this.list_01.push(e.RP_IQ_RQ_OK);
              this.list_02.push(e.CIC_LOGIN_OK);
              this.list_03.push(e.CIC_ID_IQ_OK);
              this.list_04.push(e.CIC_RP_IQ_OK);
              this.list_10.push(e.COMPLETED);
              this.list_20.push(e.LOGIN_ERROR);
              this.list_21.push(e.CIC_ID_IQ_ERROR);
              this.list_22.push(e.CIC_RP_IQ_ERROR);
              this.list_23.push(e.CIC_RQ_RS_IQ_ERROR);
              this.list_24.push(e.SCRAP_TG_RP_NOT_EXIST);
              this.list_29.push(e.OTHER_ERROR);
              this.list_ToTal.push(e.TOTAL);
            }
            this.barChartData = [
              {data: this.list_ToTal , label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];
            this.lineChartData = [
              {data: this.list_ToTal, label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];
            this.pieChartData =  [
              this.list_10[0],
              this.list_00[0],
              this.list_01[0],
              this.list_02[0],
              this.list_03[0],
              this.list_04[0],
              this.list_20[0],
              this.list_21[0],
              this.list_22[0],
              this.list_23[0],
              this.list_24[0],
              this.list_29[0]
            ];
            this.isRefreshing = false;
            this.lineChartLabels = this.listDay;
            this.barChartLabels = this.listDay;
            console.log('ok');
          } else {

          }
        }, error => {
          this.isRefreshing = false;
          this.notifyChart = error.message;
          modalNotifyChart.click();
        }
      );
    }
    if (sortBy === 'week') {
      this.chartService.getDataChartByWeek(form).subscribe(
        result => {
          if (result[0]) {
            for (const e of result) {
              this.listDay.push('Week: ' + e.DATE);
              this.list_00.push(e.SMS);
              this.list_01.push(e.RP_IQ_RQ_OK);
              this.list_02.push(e.CIC_LOGIN_OK);
              this.list_03.push(e.CIC_ID_IQ_OK);
              this.list_04.push(e.CIC_RP_IQ_OK);
              this.list_10.push(e.COMPLETED);
              this.list_20.push(e.LOGIN_ERROR);
              this.list_21.push(e.CIC_ID_IQ_ERROR);
              this.list_22.push(e.CIC_RP_IQ_ERROR);
              this.list_23.push(e.CIC_RQ_RS_IQ_ERROR);
              this.list_24.push(e.SCRAP_TG_RP_NOT_EXIST);
              this.list_29.push(e.OTHER_ERROR);
              this.list_ToTal.push(e.TOTAL);
            }
            this.barChartData = [
              {data: this.list_ToTal , label: 'Total'},
              {data: this.list_10, label: 'Completed(10)'},
              {data: this.list_00, label: '00'},
              {data: this.list_01, label: '01'},
              {data: this.list_02, label: '02'},
              {data: this.list_03, label: '03'},
              {data: this.list_04, label: '04'},
              {data: this.list_20, label: '20'},
              {data: this.list_21, label: '21'},
              {data: this.list_22, label: '22'},
              {data: this.list_23, label: '23'},
              {data: this.list_24, label: '24'},
              {data: this.list_29, label: '29'},
            ];
            this.lineChartData = [];
            this.pieChartData =  [
              this.list_10[0],
              this.list_00[0],
              this.list_01[0],
              this.list_02[0],
              this.list_03[0],
              this.list_04[0],
              this.list_20[0],
              this.list_21[0],
              this.list_22[0],
              this.list_23[0],
              this.list_24[0],
              this.list_29[0]
            ];
            this.isRefreshing = false;
            this.lineChartLabels = this.listDay;
            this.barChartLabels = this.listDay;
            console.log('ok');
          } else {

          }
        }, error => {
          this.isRefreshing = false;
          this.notifyChart = error.message;
          modalNotifyChart.click();
        }
      );
    }
  }

  changeTypeChart() {
    const type = this.typeChart.value;
    if (type === 'bar') {
      this.isChartBar = true;
      this.isChartLine = false;
      this.isChartPie = false;
    } else if (type === 'line') {
      this.isChartBar = false;
      this.isChartLine = true;
      this.isChartPie = false;
    } else if (type === 'pie') {
      this.isChartBar = false;
      this.isChartLine = false;
      this.isChartPie = true;
    }
  }

  checkTypeDate() {
    const typeDate = this.sortByDate.value;
    console.log(typeDate);
    if (typeDate === 'week') {
      this.isWeek = true;
    } else {
      this.isWeek = false;
    }
  }
}
