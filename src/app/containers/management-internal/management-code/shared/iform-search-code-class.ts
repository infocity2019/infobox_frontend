export interface IFormSearchCodeClass {
  codeClassification?: string;
  codeClassificationName?: string;
  currentLocation?: any;
  limitRow?: any;
}
