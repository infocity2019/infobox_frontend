export interface IFormCreateUpdateCode {
  code?: string;
  codeClass?: string;
  validStartDt?: string;
  validEndDt?: string;
  codeNm?: string;
  codeNmEn?: string;
  prtCdClass?: string;
  prtCd?: string;
  sysDt?: string;
  workID?: string;
  status?: number;
}
