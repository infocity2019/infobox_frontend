export interface ICodeClass {
  CODE?: string;
  CD_CLASS?: string;
  CODE_NM?: string;
  CODE_NM_EN?: string;
}
