export interface ICode {
  CODE?: string;
  CD_CLASS?: string;
  CODENM?: string;
  CODENM_EN?: string;
  PRT_CD_CLASS?: string;
  PRT_CODE?: string;
  SYS_DTIM?: string;
  WORK_ID?: string;
  VALID_START_DT?: string;
  VALID_END_DT?: string;
  STATUS?: number;
}
