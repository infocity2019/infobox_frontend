import {ICode} from './icode';

export interface ICodePagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    ICode
  ];
}
