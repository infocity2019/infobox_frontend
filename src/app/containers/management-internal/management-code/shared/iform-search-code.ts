export interface IFormSearchCode {
  code?: string;
  codeClass?: string;
  codeNm?: string;
  status: number[];
  currentLocation?: any;
  limitRow?: any;
}
