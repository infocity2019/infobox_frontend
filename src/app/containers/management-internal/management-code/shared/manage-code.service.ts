import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {IFormSearchCodeClass} from './iform-search-code-class';
import {Observable} from 'rxjs';
import {ICodeClass} from './icode-class';
import {tap} from 'rxjs/operators';
import {IFormSearchCode} from './iform-search-code';
import {ICode} from './icode';
import {IFormCreateUpdateCode} from './iform-create-update-code';
import {ICodeClassPagination} from './ICodeClassPagination';
import {ICodePagination} from './ICodePagination';

@Injectable({
  providedIn: 'root'
})
export class ManageCodeService {
  private codeAPI = this.httpG.make('/code');
  constructor(private httpG: HttpServiceGenerator) { }

  getCodeClassification(form: IFormSearchCodeClass): Observable<ICodeClassPagination> {
    return this.codeAPI.Get<ICodeClassPagination>('getCodeClassification', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  getCode(form: IFormSearchCode): Observable<ICodePagination> {
    return this.codeAPI.Post<ICodePagination>('getCode', {} , form ).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  insertCode(form: IFormCreateUpdateCode): Observable<any> {
    return this.codeAPI.Post<any>('insertCode', {}, form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  updateCode(form: IFormCreateUpdateCode): Observable<any> {
    return this.codeAPI.Put<any>('editCode', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
