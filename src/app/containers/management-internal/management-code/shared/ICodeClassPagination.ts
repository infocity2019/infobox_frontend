import {ICodeClass} from './icode-class';

export interface ICodeClassPagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    ICodeClass
  ];
}
