import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ManageCodeService} from './shared/manage-code.service';
import {IFormSearchCodeClass} from './shared/iform-search-code-class';
import {ICodeClass} from './shared/icode-class';
import {IFormSearchContract} from '../../management-customers/management-contract/shared/IFormSearchContract';
import {IFormSearchCode} from './shared/iform-search-code';
import {ICode} from './shared/icode';
import {IFormSearchCust} from '../../management-customers/management-customer/shared/IFormSearchCust';
import {IFormCreateUpdateCode} from './shared/iform-create-update-code';
import {DatePipe} from '@angular/common';
import {ICodeClassPagination} from './shared/ICodeClassPagination';
import {PagerService} from '../../../shared/service/pager.service';
import {ICodePagination} from './shared/ICodePagination';
import * as _ from 'lodash';
import {CheckRoleService} from '../../../shared/service/check-role.service';

@Component({
  selector: 'app-management-code',
  templateUrl: './management-code.component.html',
  styleUrls: ['./management-code.component.scss']
})
export class ManagementCodeComponent implements OnInit {
  searchCodeClassForm = new FormGroup({
    codeClassification: new FormControl('11111'),
    codeClassificationName: new FormControl('')
  });
  searchCodeForm = new FormGroup({
    code: new FormControl(''),
    codeClass: new FormControl(''),
    codeNm: new FormControl(''),
    status: new FormControl(''),
  });

  formRegisterCode = new FormGroup({
    code: new FormControl(''),
    codeClass: new FormControl(''),
    validStartDt: new FormControl(''),
    validEndDt: new FormControl(''),
    codeNm: new FormControl(''),
    codeNmEn: new FormControl(''),
    prtCdClass: new FormControl(''),
    prtCd: new FormControl(''),
    sysDt: new FormControl(''),
    workID: new FormControl(''),
  });

  formUpdateCode = new FormGroup({
    CODE: new FormControl(''),
    CD_CLASS: new FormControl(''),
    CODENM: new FormControl(''),
    CODENM_EN: new FormControl(''),
    PRT_CD_CLASS: new FormControl(''),
    PRT_CODE: new FormControl(''),
    SYS_DTIM: new FormControl(''),
    WORK_ID: new FormControl(''),
    VALID_START_DT: new FormControl(''),
    VALID_END_DT: new FormControl(''),
    STATUS: new FormControl('')
  });

  listCodeClass: ICodeClassPagination;
  currentLocation = 0;
  limitRow = 10;
  mesQuery: string;
  timeQuery: string;
  listCode: ICodePagination;
  hiddenNext: boolean;
  hiddenPre: boolean;
  messageValidRes: string;
  code: ICode;
  messUpdate: string;
  isLooking: boolean;
  isLookingCodeClass: boolean;
  isWaitingRegister: boolean;
  isWaitingUpdate: boolean;
  pagerCodeClass: any = {};
  pagerCode: any = {};
  codeClassificationModal: any;
  codeClassificationNameModal: any;
  codePagination: any;
  codeClassPagination: any;
  codeNmPagination: any;
  timeNow: string;
  currentUserName: string;
  roleAccount: string;
  private checkRoleAccount: { isAccessCaptcha: boolean; isRoleAdmin: boolean; isRoleUser: boolean };
  private statusArr: number[] = [];
  statusPagination: any;

  constructor(private codeService: ManageCodeService,
              private datePipe: DatePipe,
              private pagerService: PagerService,
              private checkRole: CheckRoleService) {
    this.currentUserName = localStorage.getItem('UserName');
    this.checkRoleAccount = this.checkRole.checkRoleAccount();
  }

  ngOnInit() {
  }

  setTimeSys() {
    this.timeNow = this.datePipe.transform(new Date(), 'yyyy/MM/dd-hh:mm:ss');
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, code: ICode) {
    this.messUpdate = '';
    this.code = code;
    openModalUpdate.click();
    this.formUpdateCode.patchValue({
      CODE: this.code.CODE,
      CD_CLASS: this.code.CD_CLASS,
      CODENM: this.code.CODENM,
      CODENM_EN: this.code.CODENM_EN,
      PRT_CD_CLASS: this.code.PRT_CD_CLASS,
      PRT_CODE: this.code.PRT_CODE,
      SYS_DTIM: this.code.SYS_DTIM,
      WORK_ID: this.code.WORK_ID,
      VALID_START_DT: this.code.VALID_START_DT,
      VALID_END_DT: this.code.VALID_END_DT,
      STATUS: this.code.STATUS
    });
  }

  editCode() {
    const {
      CODE,
      CD_CLASS,
      CODENM,
      CODENM_EN,
      PRT_CD_CLASS,
      PRT_CODE,
      SYS_DTIM,
      WORK_ID,
      VALID_START_DT,
      VALID_END_DT,
      STATUS
    } = this.formUpdateCode.value;
    this.messUpdate = '';

    if (_.isEqual(this.formUpdateCode.value, this.code)) {
      this.messUpdate = 'Nothing Change!';
      return;
    }
    const form: IFormCreateUpdateCode = {
      code: CODE,
      codeClass: CD_CLASS,
      codeNm: CODENM,
      codeNmEn: CODENM_EN,
      prtCdClass: PRT_CD_CLASS,
      prtCd: PRT_CODE,
      status: STATUS ,
      validStartDt: this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd'),
      validEndDt: this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd'),
    };
    this.isWaitingUpdate = true;
    console.log(form);
    this.codeService.updateCode(form).subscribe(
      result => {
        this.isWaitingUpdate = false;
        if (result.rowsAffected) {
          this.messUpdate = 'Updated Code';
          this.code = this.formUpdateCode.value;
          this.searchCode();
        }
      }, error => {
        this.isWaitingUpdate = false;
        this.messUpdate = error.message;
      }
    );
  }

  registerCode() {
    this.messageValidRes = '';
    const {code, codeClass, validStartDt, validEndDt, codeNm, codeNmEn, prtCdClass, prtCd, sysDt, workID} = this.formRegisterCode.value;
    if (code === '' || code == null) {
      return this.messageValidRes = 'Code require not blank!';
    }

    if (codeClass === '' || codeClass == null) {
      return this.messageValidRes = 'Code Classification require not blank!';
    }

    if (validStartDt === '' || validStartDt == null) {
      return this.messageValidRes = 'Valid Start Date require not blank!';
    }

    if (validEndDt === '' || validEndDt == null) {
      return this.messageValidRes = 'Valid End Date require not blank!';
    }

    if (validStartDt && validStartDt.length > 10) {
      this.messageValidRes = 'Valid Start Date wrong!';
      return;
    }

    if (validEndDt && validEndDt.length > 10) {
      this.messageValidRes = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(validStartDt, 'yyyy/MM/dd') > this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidRes = 'Valid Start Date wrong , Valid Start Date can not bigger than Today!';
      return;
    }

    if (this.datePipe.transform(validEndDt, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidRes = 'Valid Start Date wrong , Valid End Date must bigger than Today!';
      return;
    }
    this.isWaitingRegister = true;
    const form: IFormCreateUpdateCode = {
      code,
      codeClass,
      codeNm,
      codeNmEn,
      prtCdClass,
      prtCd,
      validStartDt: this.datePipe.transform(validStartDt, 'yyyy/MM/dd'),
      validEndDt: this.datePipe.transform(validEndDt, 'yyyy/MM/dd'),
      workID: this.currentUserName
    };
    console.log(form);
    this.codeService.insertCode(form).subscribe(
      result => {
        this.isWaitingRegister = false;
        if (result.rowsAffected) {
          this.formRegisterCode.reset('');
          this.messageValidRes = 'Register New Code Successful';
        }
      }
    );
  }

  setPageCodeClassificatio(page: number) {
    if (page < 1 || page > this.pagerCodeClass.totalPages) {
      return;
    }
    if (this.listCodeClass) {
      // get pager object from service
      this.pagerCodeClass = this.pagerService.getPager(this.listCodeClass.count[0].TOTAL, page);
    }
  }

  setPageCode(page: number) {
    if (page < 1 || page > this.pagerCode.totalPages) {
      return;
    }
    if (this.listCode) {
      // get pager object from service
      this.pagerCode = this.pagerService.getPager(this.listCode.count[0].TOTAL, page);
    }
  }

  searchCodeClassification() {
    this.isLookingCodeClass = true;
    const {codeClassification, codeClassificationName } = this.searchCodeClassForm.value;
    const form: IFormSearchCodeClass = {
      codeClassification,
      codeClassificationName,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(form);
    this.codeService.getCodeClassification(form).subscribe(
      result => {
        this.isLookingCodeClass = false;
        if (result.rowRs[0]) {
          this.codeClassificationModal = codeClassification;
          this.codeClassificationNameModal = codeClassificationName;
          this.listCodeClass = result;
          this.setPageCodeClassificatio(1);
        } else {
          this.listCodeClass = null;
        }
      }, error => {
        this.isLookingCodeClass = false;
        console.log(error);
      }
    );
  }

  CodeClassPagination(location: number) {
    const form: IFormSearchCodeClass = {
      codeClassification: this.codeClassificationModal,
      codeClassificationName: this.codeClassificationNameModal,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(form);
    this.codeService.getCodeClassification(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listCodeClass = result;
        } else {
          this.listCodeClass = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  searchCode() {
    this.statusArr = [];
    this.isLooking = true;
    this.currentLocation = 0;
    const {code, codeClass, codeNm ,status} = this.searchCodeForm.value;
    if (status == '' || status == 1) {
      this.statusArr.push(1);
    } else if (status == 3) {
      this.statusArr = [0,2,1];
    } else if (status == 2) {
      this.statusArr.push(0,2);
    }
    const searchForm: IFormSearchCode = {
      code,
      codeClass,
      codeNm,
      status: this.statusArr,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.codeService.getCode(searchForm).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.codePagination = code;
          this.codeClassPagination = codeClass;
          this.codeNmPagination = codeNm;
          this.statusPagination = this.statusArr;
          this.listCode = result;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.setPageCode(1);
        } else {
          this.mesQuery = 'Not Found!';
          this.getDate();
          this.listCode = null;
        }
      }, error => {
        this.isLooking = false;
        console.log(error);
      }
    );
  }

  paginationCode(location: number) {
    const searchForm: IFormSearchCode = {
      code: this.codePagination,
      codeClass: this.codeClassPagination,
      codeNm: this.codeNmPagination,
      status: this.statusPagination,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.codeService.getCode(searchForm).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listCode = result;
        } else {
          this.listCode = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }
  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  getValueToInputOrga(codeClass: ICodeClass, closeModalSearchOrga: HTMLButtonElement) {
    this.searchCodeForm.patchValue({
      codeClass: codeClass.CODE,
    });
    closeModalSearchOrga.click();
  }

  getValueToInputOrgaRegister(codeClass: ICodeClass, closeModalSearchOrgaRegister: HTMLButtonElement) {
    this.formRegisterCode.patchValue({
      codeClass: codeClass.CD_CLASS
    });
    closeModalSearchOrgaRegister.click();
  }

  removeMes() {
    this.messageValidRes = '';
    this.setTimeSys();
  }
}
