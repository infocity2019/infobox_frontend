import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCodeComponent } from './management-code.component';

describe('ManagementCodeComponent', () => {
  let component: ManagementCodeComponent;
  let fixture: ComponentFixture<ManagementCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
