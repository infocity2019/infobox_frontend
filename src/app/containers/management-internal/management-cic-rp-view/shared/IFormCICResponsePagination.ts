import {IFormCICResponse} from './iform-cicresponse';

export interface IFormCICResponsePagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IFormCICResponse
  ];
}
