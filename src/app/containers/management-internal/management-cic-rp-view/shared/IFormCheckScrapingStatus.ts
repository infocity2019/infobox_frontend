export interface IFormCheckScrapingStatus {
  appCd?: any;
  iftUrl?: any;
  orgCd?: any;
  svcCd?: any;
  dispNm?: any;
  userId?: any;
  userPw?: any;
  customerType?: any;
  cicNo?: any;
  taxNo?: any;
  cmtNo?: any;
  reportType?: any;
  voteNo?: any;
  reqStatus?: any;
  inqDt1?: any;
  inqDt2?: any;
  step_input?: any;
  step_data?: any;
  sendTime?: any;
  reportCicNo?: any;
  niceSessionKey?: any;
  dataCic?: any;
}
