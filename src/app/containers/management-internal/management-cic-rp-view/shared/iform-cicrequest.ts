export interface IFormCICRequest {
  niceSsID?: string;
  orgCode?: string;
  productCode?: string;
  taxCode?: string;
  nationalID?: string;
  oldNationalID?: string;
  passportNO?: string;
  phoneNumber?: string;
  inqDateFrom?: string;
  inqDateTo?: string;
  scrapStatus?: string;
  currentLocation?: any;
  limitRow?: any;
}
