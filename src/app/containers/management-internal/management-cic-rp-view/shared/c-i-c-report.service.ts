import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {IFormCICRequest} from './iform-cicrequest';
import {IFormCICResponse} from './iform-cicresponse';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {IFormCICResponsePagination} from './IFormCICResponsePagination';
import {IB003Req} from '../../../manual-captcha-b003/shared/ib003-req';
import {IB003Res} from '../../../manual-captcha-b003/shared/ib003-res';
import {IFormCheckScrapingStatus} from './IFormCheckScrapingStatus';

@Injectable({
  providedIn: 'root'
})
export class CICReportService {
  private CICRpApi = this.httpG.make('/cicreport');
  private internalAPI = this.httpG.makeInternal('internal');
  constructor(private httpG: HttpServiceGenerator) { }

  getHistoryCICReport(form: IFormCICRequest): Observable<IFormCICResponsePagination> {
    return this.CICRpApi.Get<IFormCICResponsePagination>('viewHistoryCICReport', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  SendDataB003(form: IFormCheckScrapingStatus): Observable<IB003Res> {
    return this.internalAPI.Post<IB003Res>('cicB0003', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e)
      }
    ))
  }

  checkRecordToExecuteManual(niceSsID: IFormCICRequest): Observable<any> {
    return this.CICRpApi.Get<any>('checkRecordToExecuteManual', niceSsID).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
