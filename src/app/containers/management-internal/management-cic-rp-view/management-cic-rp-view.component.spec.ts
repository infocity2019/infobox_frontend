import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCicRpViewComponent } from './management-cic-rp-view.component';

describe('ManagementCicRpViewComponent', () => {
  let component: ManagementCicRpViewComponent;
  let fixture: ComponentFixture<ManagementCicRpViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementCicRpViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCicRpViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
