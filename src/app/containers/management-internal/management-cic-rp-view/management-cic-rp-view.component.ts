import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {IProduct} from '../../management-customers/management-contract/shared/iproduct';
import {IFormSeachProduct} from '../../management-customers/management-contract/shared/IFormSeachProduct';
import {ContractService} from '../../management-customers/management-contract/shared/contract.service';
import {ManagementContractService} from '../../management-customers/management-contract/shared/management-contract.service';
import {IFormSearchCust} from '../../management-customers/management-customer/shared/IFormSearchCust';
import {ManageCustomerService} from '../../management-customers/management-customer/shared/manageCustomer.service';
import {ICustInfo} from '../../management-customers/management-customer/shared/ICustInfo';
import {IFormCICRequest} from './shared/iform-cicrequest';
import {DatePipe} from '@angular/common';
import {CICReportService} from './shared/c-i-c-report.service';
import {IFormCICResponse} from './shared/iform-cicresponse';
import {IFormSearchContract} from '../../management-customers/management-contract/shared/IFormSearchContract';
import {PagerService} from '../../../shared/service/pager.service';
import {ISearchProductPagination} from '../../management-customers/management-contract/shared/ISearchProductPagination';
import {IcustInfoArray} from '../../management-customers/management-customer/shared/IcustInfoArray';
import {IFormCICResponsePagination} from './shared/IFormCICResponsePagination';
import {IB003Req} from '../../manual-captcha-b003/shared/ib003-req';
import {IFormCheckScrapingStatus} from './shared/IFormCheckScrapingStatus';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-management-cic-rp-view',
  templateUrl: './management-cic-rp-view.component.html',
  styleUrls: ['./management-cic-rp-view.component.scss']
})
export class ManagementCicRpViewComponent implements OnInit {
  isOpenViewButtonRP = false;
  CICHistory: IFormCICResponse;
  formSearchCICRp = new FormGroup({
    orgClass: new FormControl(''),
    orgName: new FormControl(''),
    orgCode: new FormControl(''),
    productCode: new FormControl(''),
    taxCode: new FormControl(''),
    nationalID: new FormControl(''),
    oldNationalID: new FormControl(''),
    passportNO: new FormControl(''),
    phoneNumber: new FormControl(''),
    inqDateFrom: new FormControl(null),
    inqDateTo: new FormControl(null),
    scrapStatus: new FormControl(''),
  });

  formSearchProductCode = new FormGroup({
    productCode: new FormControl(''),
    productNM: new FormControl('')
  });

  formSearchCusModal = new FormGroup({
    custClassicfication: new FormControl(''),
    cusCd: new FormControl(''),
    custNm: new FormControl(''),
  });
  listProduct: ISearchProductPagination;
  listCustModal: IcustInfoArray;
  currentLocation = 0;
  limitRow = 10;
  listCICRp: IFormCICResponsePagination;
  mesQuery: string;
  hiddenNext: boolean;
  timeQuery: string;
  hiddenPre: boolean;
  CICHandled: IFormCICResponse;
  handledResult: string;
  isLooking: boolean;
  isLookingPro: boolean;
  isLookingCus: boolean;
  isWaitingCIC: boolean;
  messQueryDate: string;
  pagerProduct: any = {};
  productCodeModal: any;
  productNMModal: any;
  pagerOrg: any = {};
  custClassicficationModal: any;
  cusCdModal: any;
  custNmModal: any;
  orgCodePagination: any;
  productCodePagination: any;
  CICNumberPagination: any;
  inqDateFromPagination: string;
  inqDateToPagination: string;
  pagerCIC: any = {};
  taxCodePagination: any;
  oldNationalIDPagination: any;
  passportNOPagination: any;
  nationalIDPagination: any;
  phoneNumberPagination: any;
  scrapStatusPagination: any;
  listNiceSSID: string[] = [];
  dataCIC: object[] = [];
  cmtID: string;
  handledResultErr: any;
  timeNow: string;
  processB0003: Subscription;
  checkRecordProcess: Subscription;
  constructor(private contractService: ManagementContractService,
              private cusService: ManageCustomerService,
              private cicReportService: CICReportService,
              private datePipe: DatePipe,
              private pagerService: PagerService) {
  }

  ngOnInit() {
  }

  setPageSearchProduct(page: number) {
    console.log('>>> product');
    if (page < 1 || page > this.pagerProduct.totalPages) {
      return;
    }
    if (this.listProduct) {
      // get pager object from service
      this.pagerProduct = this.pagerService.getPager(this.listProduct.count[0].TOTAL, page);
    }
  }
  setPageSearchCustModal(page: number) {
    if (page < 1 || page > this.pagerOrg.totalPages) {
      return;
    }
    if (this.listCustModal) {
      // get pager object from service
      this.pagerOrg = this.pagerService.getPager(this.listCustModal.count[0].TOTAL, page);
    }
  }

  setPageSearchCIC(page: number) {
    if (page < 1 || page > this.pagerCIC.totalPages) {
      return;
    }
    if (this.listCICRp) {
      // get pager object from service
      this.pagerCIC = this.pagerService.getPager(this.listCICRp.count[0].TOTAL, page);
    }
  }

  searchOrgByNameAndClass() {
    this.isLookingCus = true;
    const {custClassicfication, cusCd , custNm} = this.formSearchCusModal.value;
    const form: IFormSearchCust = {
      custClassicfication,
      cusCd,
      custNm,
      status: [1] ,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        this.isLookingCus = false;
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.custClassicficationModal = custClassicfication;
          this.cusCdModal = cusCd;
          this.custNmModal = custNm;
          this.listCustModal = result;
          this.setPageSearchCustModal(1);
        } else {
          this.listCustModal = null;
        }
      }, error => {
        this.isLookingCus = false;
      }
    );
  }

  paginationCustModal(location: number) {
    const form: IFormSearchCust = {
      custClassicfication: this.custClassicficationModal,
      cusCd: this.cusCdModal,
      custNm: this.custNmModal,
      status: [1] ,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listCustModal = result;
        } else {
          this.listCustModal = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  productSearch() {
    this.isLookingPro = true;
    const {productCode, productNM} = this.formSearchProductCode.value;
    const form: IFormSeachProduct = {
      productCode,
      productNM,
      currentLocation:  this.currentLocation ,
      limitRow: this.limitRow
    };
    console.log(form);
    this.contractService.getProductCode(form).subscribe(
      result => {
        this.isLookingPro = false;
        if (result.rowRs[0]) {
          this.productCodeModal = productCode;
          this.productNMModal = productNM;
          this.listProduct = result;
          this.setPageSearchProduct(1);
        } else {
          this.listProduct = null;
        }
      }, error => {
        this.isLookingPro = false;
        console.log(error);
      }
    );
  }

  paginationProduct(location: number) {
    console.log(location);
    const form: IFormSeachProduct = {
      productCode: this.productCodeModal,
      productNM: this.productNMModal,
      currentLocation: location * this.limitRow ,
      limitRow: this.limitRow
    };
    console.log(form);
    this.contractService.getProductCode(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listProduct = result;
        } else {
          this.listProduct = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  viewButtonRp(CICRp: IFormCICResponse) {
    this.CICHandled = CICRp;
    this.isOpenViewButtonRP = true;
  }

  searchHistoryCICRp() {
    this.isOpenViewButtonRP = false;
    this.messQueryDate = '';
    this.isLooking = true;
    const {
      orgCode,
      productCode,
      taxCode,
      nationalID,
      oldNationalID,
      passportNO,
      phoneNumber,
      inqDateFrom,
      inqDateTo,
      scrapStatus
    } = this.formSearchCICRp.value;

    if (inqDateFrom && !inqDateTo || !inqDateFrom && inqDateTo) {
      this.isLooking = false;
      return this.messQueryDate = ' Empty date query!';
    }

    const form: IFormCICRequest = {
      orgCode,
      productCode,
      taxCode,
      nationalID,
      oldNationalID,
      passportNO,
      phoneNumber,
      inqDateFrom: this.datePipe.transform(inqDateFrom, 'yyyy/MM/dd'),
      inqDateTo: this.datePipe.transform(inqDateTo, 'yyyy/MM/dd') ,
      scrapStatus,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(form);
    this.cicReportService.getHistoryCICReport(form).subscribe(
      result =>  {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.orgCodePagination = orgCode;
          this.productCodePagination = productCode;
          this.taxCodePagination = taxCode;
          this.nationalIDPagination = nationalID;
          this.oldNationalIDPagination = oldNationalID;
          this.passportNOPagination = passportNO;
          this.phoneNumberPagination = phoneNumber;
          this.inqDateFromPagination = form.inqDateFrom;
          this.inqDateToPagination = form.inqDateTo;
          this.scrapStatusPagination = scrapStatus;
          this.listCICRp = result;
          this.setPageSearchCIC(1);
          this.mesQuery = 'Successful!';
          this.getDate();
        } else {
          this.getDate();
          this.mesQuery = 'Not Found!';
          this.listCICRp = null;
        }
      }, error => {
        this.isLooking = false;
      }
    );
  }

  paginationCICRp(location: number) {
    this.isOpenViewButtonRP = false;
    const form: IFormCICRequest = {
      orgCode: this.orgCodePagination,
      productCode: this.productCodePagination,
      taxCode: this.taxCodePagination,
      nationalID: this.nationalIDPagination,
      oldNationalID: this.oldNationalIDPagination,
      passportNO: this.passportNOPagination,
      phoneNumber: this.phoneNumberPagination,
      inqDateFrom: this.inqDateFromPagination,
      inqDateTo: this.inqDateToPagination ,
      scrapStatus: this.scrapStatusPagination,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(form);
    this.cicReportService.getHistoryCICReport(form).subscribe(
      result =>  {
        if (result.rowRs[0]) {
          this.listCICRp = result;
        } else {
          this.listCICRp = null;
        }
      }, error => {
      }
    );
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, cic: IFormCICResponse) {
    this.CICHistory = cic;
    openModalUpdate.click();
  }

  getValueProductCode(product: IProduct, closeSearchProductCodeModal: HTMLButtonElement) {
    this.formSearchCICRp.patchValue({
      productCode: product.CODE
    });
    closeSearchProductCodeModal.click();
  }

  getDataToInputSearch(closeOrnName: HTMLButtonElement, cust: ICustInfo) {
    this.formSearchCICRp.patchValue({
      orgClass: cust.CUST_GB,
      orgName: cust.CUST_NM_ENG,
      orgCode: cust.CUST_CD,
    });
    closeOrnName.click();
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  handledViewRp(modalHandledCIC: HTMLButtonElement) {
    if (this.CICHandled.SCRP_STAT_CD == '10') {
      modalHandledCIC.click();
      return this.handledResult = 'Successfully';
    } else {
      this.timeNow = this.datePipe.transform(new Date(), 'yyyyMMdd');
      this.listNiceSSID = [];
      this.dataCIC = [];
      this.isWaitingCIC = true;
      this.handledResult = '';
      modalHandledCIC.click();
      console.log(this.CICHandled);
      const form: IFormCICRequest = {
        niceSsID: this.CICHandled.NICE_SSIN_ID
      };
      this.checkRecordProcess = this.cicReportService.checkRecordToExecuteManual(form).subscribe(
        result => {
          if (result[0]) {
            this.listNiceSSID.push(result[0].NICE_SSIN_ID);
            const obj = {
              NICESESSIONKEY: result[0].NICE_SSIN_ID,
              CICID: result[0].S_CIC_NO,
              INQ_DTIM: result[0].INQ_DTIM,
              LOGIN_ID: result[0].LOGIN_ID,
              LOGIN_PW: result[0].LOGIN_PW,
              SYS_DTIM: result[0].SYS_DTIM
            };
            this.dataCIC.push(obj);
            const form: IFormCheckScrapingStatus = {
              appCd: 'INFOBOX',
              iftUrl: 'http://www.infotech3.co.kr/ift/deploy/',
              orgCd: 'cic.vn',
              svcCd: 'B0003',
              dispNm: 'cic.org.vn',
              userId: result[0].S_USER_ID,
              userPw: atob(result[0].S_USER_PW),
              customerType: '2',
              cicNo: '',
              taxNo: '',
              cmtNo: '',
              step_input: '',
              step_data: '',
              reportType: '06',
              voteNo: '111',
              niceSessionKey: this.listNiceSSID,
              inqDt1: result[0].INQ_DTIM,
              inqDt2: result[0].INQ_DTIM,
              reportCicNo: result[0].S_CIC_NO,
              sendTime: '',
              dataCic: this.dataCIC
            };
            console.log(form);
            this.processB0003 = this.cicReportService.SendDataB003(form).subscribe(
              result => {
                console.log('done');
                this.isWaitingCIC = false;
                this.handledResult = 'Successfully';
                this.searchHistoryCICRp();
              }, error => {
                console.log('fail');
                this.isWaitingCIC = false;
                this.handledResult = 'Fail';
              }
            );
          } else {
            //[] need run B0002
            this.isWaitingCIC = false;
            this.handledResult = 'This record need to process B0002 first';
          }
        }, error => {
          this.handledResultErr = error.message;
        }
      );
    }
  }

  stopProcessScrapRP() {
    if (this.processB0003) {
      this.processB0003.unsubscribe();
    }
    if (this.checkRecordProcess) {
      this.checkRecordProcess.unsubscribe();
    }
    this.isWaitingCIC = false;
  }
}
