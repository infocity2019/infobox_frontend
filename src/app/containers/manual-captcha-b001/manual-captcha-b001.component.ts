import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {IResponseListB1003} from './shared/iresponse-list-b1003';
import {B1003Service} from './shared/b1003.service';
import {DatePipe} from '@angular/common';
import {IB0001Request} from './shared/ib0001-request';
import {IB0001Response} from './shared/ib0001-response';
import {IRquestGetListB1003} from './shared/IRquestGetListB1003';
import {PagerService} from '../../shared/service/pager.service';
import {IResponseListB1003Pagination} from './shared/IResponseListB1003Pagination';


@Component({
  selector: 'app-manual-captcha-b001',
  templateUrl: './manual-captcha-b001.component.html',
  styleUrls: ['./manual-captcha-b001.component.css']
})
export class ManualCaptchaB001Component implements OnInit {
  timeQuery: string;
  isLooking: boolean;
  currentLocation = 0;
  formSearchCIC = new FormGroup({
    niceSsID: new FormControl(''),
  });
  limitRow = 10;
  mesQuery: string;
  listB1003: IResponseListB1003Pagination;
  hiddenNext: boolean;
  hiddenPre: boolean;
  captchaInput: string;
  messCaptcha: string;
  B1003: IResponseListB1003;
  B1003ResponseFromInternal: IB0001Response;
  cmtID: string;
  isSubmitting: boolean;
  inputCaptcha: string;
  niceSSID: string[] = [];
  isResult: boolean;
  pagerB1003: any = {};
  niceSSIDPagination: any;

  constructor(private b1003Service: B1003Service,
              private datePipe: DatePipe,
              private pagerService: PagerService) { }

  ngOnInit(): void {
  }
  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  searchDataB1003ByInternal() {
    this.niceSSID = [];
    this.isResult = false;
    if (this.B1003) {
      if (this.B1003.NATL_ID) {
        this.cmtID = this.B1003.NATL_ID;
      } else if (this.B1003.OLD_NATL_ID) {
        this.cmtID = this.B1003.OLD_NATL_ID;
      } else if (this.B1003.PSPT_NO) {
        this.cmtID = this.B1003.PSPT_NO;
      } else {
        console.log('cmt null');
        return;
      }
      if (!this.inputCaptcha && this.B1003ResponseFromInternal) {
        return this.messCaptcha = 'Captcha can not blank!';
      }
      this.niceSSID.push(this.B1003.NICE_SSIN_ID);
      this.isSubmitting = true;
      const form: IB0001Request = {
        appCd: 'INFOBOX',
        iftUrl: 'http://www.infotech3.co.kr/ift/deploy/',
        orgCd: 'cic.vn',
        svcCd: 'B1003',
        dispNm: 'cic.org.vn',
        userId: this.B1003.LOGIN_ID,
        userPw: atob(this.B1003.LOGIN_PW).substr(14),
        customerType: '2',
        cicNo: '',
        cmtNo: this.cmtID,
        taxNo: '',
        reportType: '06',
        step_useYn: 'Y',
        step_input: this.inputCaptcha ? this.inputCaptcha : '',
        step_data: this.B1003ResponseFromInternal ? this.B1003ResponseFromInternal.dataStep : {},
        voteNo: '',
        reqStatus: '',
        inqDt1: this.B1003.S_INQ_DT1,
        inqDt2: this.B1003.S_INQ_DT2,
        niceSessionKey: this.niceSSID
      };
      console.log(form);
      this.b1003Service.getB1003ByInternal(form).subscribe(
        result => {
          this.isSubmitting = false;
          if (result.imgBase64) {
            this.B1003ResponseFromInternal = result;
            this.inputCaptcha = '';
          } else if (result.outJson.errMsg) {
            this.messCaptcha = result.outJson.errMsg;
          } else if (result.outJson.outB0002.errMsg) {
            this.messCaptcha = result.outJson.outB0002.errMsg;
          } else if (result.outJson.outB0003.errMsg) {
            this.messCaptcha = result.outJson.outB0002.errMsg;
          } else {
            this.B1003ResponseFromInternal = result;
            this.isResult = true;
          }
          // else if (result.outJson.outB0003.list) {
          //   this.listB003AfterSendCaptcha = result.outJson.outB0003.list;
          // } else if (result.outJson.outB0003.list.length == 0) {
          //   this.messCaptcha = 'No result found!';
          // }
        } , error => {
          this.isSubmitting = false;
          this.messCaptcha = error.message;
        }
      );
    }
  }

  setPageSearchB1003(page: number) {
    if (page < 1 || page > this.pagerB1003.totalPages) {
      return;
    }
    if (this.listB1003) {
      // get pager object from service
      this.pagerB1003 = this.pagerService.getPager(this.listB1003.count[0].TOTAL, page);
    }
  }

  searchDataForB1003() {
    this.isLooking = true;
    const {
      niceSsID
    } = this.formSearchCIC.value;

    const form: IRquestGetListB1003 = {
      niceSsID,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(form);
    this.b1003Service.getDataForB1003(form).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.niceSSIDPagination = niceSsID;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.listB1003 = result;
          this.setPageSearchB1003(1);
        } else {
          this.mesQuery = 'Not Found!';
          this.getDate();
          this.listB1003 = null;
        }
      }, error => {
        this.isLooking = false;
      }
    );
  }

  paginationB1003(location: number) {
    const form: IRquestGetListB1003 = {
      niceSsID: this.niceSSIDPagination,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(form);
    this.b1003Service.getDataForB1003(form).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.listB1003 = result;
        } else {
          this.listB1003 = null;
        }
      }
    );
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, b1003: IResponseListB1003) {
    openModalUpdate.click();
    this.inputCaptcha = '';
    this.messCaptcha = '';
    this.B1003 = b1003;
    this.B1003ResponseFromInternal = null;
    this.isResult = false;
    // this.resCaptcha = null;
  }
}
