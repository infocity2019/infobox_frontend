import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../shared/service';
import {IRquestGetListB1003} from './IRquestGetListB1003';
import {Observable} from 'rxjs';
import {IResponseListB1003} from './iresponse-list-b1003';
import {tap} from 'rxjs/operators';
import {IB0001Request} from './ib0001-request';
import {IB0001Response} from './ib0001-response';
import {IResponseListB1003Pagination} from './IResponseListB1003Pagination';

@Injectable({
  providedIn: 'root'
})
export class B1003Service {
  private internalAPI = this.httpG.makeInternal('/internal');
  private backofficeAPI = this.httpG.make('/manualCaptcha');
  constructor(private httpG: HttpServiceGenerator) { }

  getDataForB1003(form: IRquestGetListB1003): Observable<IResponseListB1003Pagination> {
    return this.backofficeAPI.Get<IResponseListB1003Pagination>('getListB001', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  getB1003ByInternal(form: IB0001Request): Observable<IB0001Response> {
    return this.internalAPI.Post<IB0001Response>('cicB0003' , {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
