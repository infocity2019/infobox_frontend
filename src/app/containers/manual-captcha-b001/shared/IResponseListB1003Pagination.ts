import {IResponseListB1003} from './iresponse-list-b1003';

export interface IResponseListB1003Pagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IResponseListB1003
  ];
}
