export interface IB0001Response {
  imgBase64?: any;
  dataStep?: {
    cookie?: {
      CICORGVN?: any;
      LB_cookies?: any;
    },
    step?: any;
    ViewState?: any;
    ref?: any;
    state?: any;
    afrLoop?: any;
  };
  workerAppVer?: any;
  outJson?: {
    in?: {
      dispNm?: any;
      voteNo?: any;
      appCd?: any;
      svcCd?: any;
      userId?: any;
      cmtNo?: any;
      reportType?: any;
      customerType?: any;
      thread_id?: any;
      reqStatus?: any;
      orgCd?: any;
      iftUrl?: any;
      userPw?: any;
      step_data?: any;
      inqDt2?: any;
      cicNo?: any;
      step_useYn?: any;
      inqDt1?: any;
      taxNo?: any;
      step_input?: any;
      engVer?: any;
    },
    errMsg?: any;
    appCd?: any;
    svcCd?: any;
    resMsg?: any;
    outA0001?: {
      errYn?: any;
      errMsg?: any;
    },
    outB0002?: {
      errYn?: any;
      errMsg?: any;
    },
    outB1003?: {
      errYn?: any;
      errMsg?: any;
      cicNo?: any;
      name?: any;
      address?: any;
      numberOfFi?: any;
      cautionsLoanYn?: any;
      badLoanYn?: any;
      baseDate?: any;
      warningGrage?: any;
      reportComment?: any;
      reportHtml?: any;
    },
    uid?: any;
    outB0003?: {
      errYn?: any;
      errMsg?: any;
    },
    outB0001?: {
      errYn?: any;
      errMsg?: any;
      list?: [
        {
          cicNo?: any;
          name?: any;
          address?: any;
          taxNo?: any;
          bizReg?: any;
          cmtNo?: any;
        },
      ]
    },
    orgCd?: any;
    resCd?: any;
    errYn?: any;
  };
  workerOsNm?: any;
  resCd?: any;
  reqCd?: any;
  resMsg?: any;
  workerHostNm?: any;
  workerResDt?: any;
  workerReqDt?: any;
}
