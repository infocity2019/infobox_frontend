export interface IB0001Request {
  appCd: string;
  iftUrl: string;
  orgCd: string;
  svcCd: string;
  dispNm: string;
  userId: string;
  userPw: string;
  customerType:string;
  cicNo?: string;
  cmtNo:string;
  taxNo?:string;
  reportType:string;
  step_useYn:string;
  step_input?:string;
  step_data?: any,
  voteNo?: string;
  reqStatus?: string;
  inqDt1:string;
  inqDt2:string;
  niceSessionKey:any;
}
