import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualCaptchaB001Component } from './manual-captcha-b001.component';

describe('ManualCaptchaB001Component', () => {
  let component: ManualCaptchaB001Component;
  let fixture: ComponentFixture<ManualCaptchaB001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualCaptchaB001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualCaptchaB001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
