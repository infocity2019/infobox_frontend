import { Component, OnInit } from '@angular/core';
import { FPTBalanceService } from './shared/FPTBalance.service';

@Component({
	selector: 'app-FPTBalance',
	templateUrl: './FPTBalance.component.html',
	styleUrls: ['./FPTBalance.component.scss']
})
export class FPTBalanceComponent implements OnInit {
	constructor(private balanceService: FPTBalanceService) { }
	username =  "nice"
	password = "nice@123"
	responseToken: any
	response : any
	ngOnInit() {
		this.getBalanceData()
	}

	getBalanceData() {
		this.balanceService
			.getToken({
				username: this.username,
				password: this.password
			})
			.subscribe(
				res => {
					if (res == null) return;
					this.responseToken = res;
						//console.log(this.responseToken)
					if(this.responseToken.ErrorCode == 0){
						console.log(this.responseToken.Data.sessionToken)
						this.balanceService.getRequestExpenseBalance(this.responseToken.Data.sessionToken)
							.subscribe(
								res => {
									if (res == null) return;
									this.response = res;
										//console.log(this.response)
									if(this.response.ErrorCode == 0){
										//console.log(this.response.Data)
									}
								},
								err => {
									console.log(err.error.error_description);
								}
							);	
					}
				},
				err => {
					console.log(err.error.error_description);
				}
			);
	}
}
