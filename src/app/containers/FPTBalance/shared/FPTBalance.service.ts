import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class FPTBalanceService extends HttpClient {
    constructor(handler: HttpHandler) {
        super(handler);
    }
  
/*
curl -X GET "https://api.trandata.io/aggregator/api/v1/home/getRequestExpenseBalance" \
-H "Authorization: Bearer YOUR_JWT_TOKEN_FROM_LOGIN_API"
 */
	URL_TRANDATA_LOGIN = 'https://api.trandata.io/account/unauth/v1/login' 
	URL_TRANDATA_BALANCE = 'https://api.trandata.io/aggregator/api/v1/home/getRequestExpenseBalance' 
	public getToken(body: any) {
        return super.post(this.URL_TRANDATA_LOGIN, body);
    }

	public getRequestExpenseBalance(access_token: any): Observable<any> {
		let headers = new HttpHeaders();
        headers = headers.append('Authorization', 'Bearer ' + access_token);
        return super.get(this.URL_TRANDATA_BALANCE, {headers});
    }
}
