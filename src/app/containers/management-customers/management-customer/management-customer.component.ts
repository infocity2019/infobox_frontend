import { Component, OnInit } from '@angular/core';
import {ManageCustomerService} from './shared/manageCustomer.service';
import {IFormSearchCust} from './shared/IFormSearchCust';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ICustInfo} from './shared/ICustInfo';
import {IFormCreateUpdateCus} from './shared/IFormCreateUpdateCus';
import {DatePipe} from '@angular/common';
import {IcustInfoArray} from './shared/IcustInfoArray';
import {PagerService} from '../../../shared/service/pager.service';
import * as _ from 'lodash';
import {error} from 'util';
import {CheckRoleService} from '../../../shared/service/check-role.service';

@Component({
  selector: 'app-management-customer',
  templateUrl: './management-customer.component.html',
  styleUrls: ['./management-customer.component.scss']
})
export class ManagementCustomerComponent implements OnInit {
  formSearchCus = new FormGroup({
    custClassicfication: new FormControl(''),
    cusCd: new FormControl(''),
    custNm: new FormControl(''),
    status: new FormControl(''),
  });

  formSearchCusModal = new FormGroup({
    custClassicfication: new FormControl(''),
    cusCd: new FormControl(''),
    custNm: new FormControl(''),
  });
  formAddCus = new FormGroup({
    classFication: new FormControl('', Validators.required),
    custCD: new FormControl('', Validators.required),
    custNM: new FormControl(''),
    custNMENG: new FormControl(''),
    custBranchNM: new FormControl(''),
    custBranchNM_EN: new FormControl(''),
    coRgstNo: new FormControl(''),
    industryCD: new FormControl(''),
    prtOrganizationClass: new FormControl(''),
    prtOrganizationCD: new FormControl(''),
    addr: new FormControl(''),
    validStartDT: new FormControl(''),
    validEndDT: new FormControl(''),
    operationDate: new FormControl('', Validators.required),
    userID: new FormControl('')
  });

  formUpdateCus = new FormGroup({
    CUST_GB: new FormControl('', Validators.required),
    CUST_CD: new FormControl('', Validators.required),
    CUST_NM: new FormControl(''),
    CUST_NM_ENG: new FormControl(''),
    BRANCH_NM: new FormControl(''),
    BRANCH_NM_ENG: new FormControl(''),
    CO_RGST_NO: new FormControl(''),
    BIZ_CG_CD: new FormControl(''),
    PRT_CUST_GB: new FormControl(''),
    PRT_CUST_CD: new FormControl(''),
    ADDR: new FormControl(''),
    VALID_START_DT: new FormControl(''),
    VALID_END_DT: new FormControl(''),
    SYS_DTIM: new FormControl('', Validators.required),
    WORK_ID: new FormControl(''),
    STATUS: new FormControl('')
  });

  timeNow: any;
  currentUserName: string;
  listInfoCus: IcustInfoArray;
  listCustModal: IcustInfoArray;
  custInfo: ICustInfo;
  messageValidADD = '';
  currentLocation = 0;
  limitRow = 10;
  hiddenNext: boolean;
  hiddenPre: boolean;
  mesModal = '';
  messageValidUpdate = '';
  timeQuery: string;
  mesQuery: string;
  isLooking: boolean;
  isLookingOrg: boolean;
  isWaitingRegister: boolean;
  isWaitingUpdate: boolean;
  pagerCust: any = {};
  pagerCustModal: any = {};
  pagedItems: any[];
  custClassicficationModal: any;
  cusCdModal: any;
  custNmModal: any;
  custClassicfication: any;
  cusCd: any;
  custNm: any;
  statusCust: any;
  roles: [];
  private checkRoleAccount: { isAccessCaptcha: boolean; isRoleAdmin: boolean; isRoleUser: boolean };
  statusArr: number[] = [];
  constructor(
    private cusService: ManageCustomerService,
    private datePipe: DatePipe,
    private pagerService: PagerService,
    private checkRole: CheckRoleService
  ) {
    this.currentUserName = localStorage.getItem('UserName');
    this.checkRoleAccount = this.checkRole.checkRoleAccount();
  }


  ngOnInit() {
  }

  setPageSearchCust(page: number) {
    if (page < 1 || page > this.pagerCust.totalPages) {
      return;
    }
    if (this.listInfoCus) {
    // get pager object from service
    this.pagerCust = this.pagerService.getPager(this.listInfoCus.count[0].TOTAL, page);
    }
  }
  setPageSearchCustModal(page: number) {
    if (page < 1 || page > this.pagerCustModal.totalPages) {
      return;
    }
    if (this.listCustModal) {
    // get pager object from service
    this.pagerCustModal = this.pagerService.getPager(this.listCustModal.count[0].TOTAL, page);
    }
  }

  searchInfoCustByNameAndClass() {
    this.isLookingOrg = true;
    const {custClassicfication, cusCd , custNm} = this.formSearchCusModal.value;
    const form: IFormSearchCust = {
      custClassicfication,
      cusCd,
      custNm,
      status: [1],
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        this.isLookingOrg = false;
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.custClassicficationModal = custClassicfication;
          this.cusCdModal = cusCd;
          this.custNmModal = custNm;
          this.listCustModal = result;
          this.setPageSearchCustModal(1);
        } else {
          this.listCustModal = null;
        }
      }, error => {
        this.isLookingOrg = false;
      }
    );
  }

  paginationCustModal(location: number) {
    const form: IFormSearchCust = {
      custClassicfication: this.custClassicficationModal,
      cusCd: this.cusCdModal,
      custNm: this.custNmModal,
      status: [1],
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listCustModal = result;
        } else {
          this.listCustModal = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  searchInfoCustomer() {
    this.statusArr = [];
    this.isLooking = true;
    this.currentLocation = 0;
    let {custClassicfication, cusCd, custNm , status} = this.formSearchCus.value;
    if (status == '' || status == 1) {
      this.statusArr.push(1);
    } else if (status == 3) {
      this.statusArr = null;
    } else if (status == 2) {
      this.statusArr.push(0);
    }
    const searchForm: IFormSearchCust = {
      custClassicfication,
      cusCd,
      custNm,
      status: this.statusArr,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.cusService.getInfoCus(searchForm).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.custClassicfication = custClassicfication;
          this.cusCd = cusCd;
          this.custNm = custNm;
          this.statusCust = this.statusArr;
          this.listInfoCus = result;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.setPageSearchCust(1);
        } else {
          this.mesQuery = 'Not Found!';
          this.getDate();
          this.listInfoCus = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  paginationCustomer(location: number) {
    const searchForm: IFormSearchCust = {
      custClassicfication: this.custClassicfication,
      cusCd: this.cusCd,
      custNm: this.custNm,
      status: this.statusCust,
      currentLocation: location * this.limitRow ,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.cusService.getInfoCus(searchForm).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listInfoCus = result;
        } else {
          this.listInfoCus = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  createNewCus() {
    this.messageValidADD = '';
    const {
      classFication,
      custCD,
      custNM,
      custNMENG,
      custBranchNM,
      custBranchNM_EN,
      coRgstNo,
      industryCD,
      prtOrganizationClass,
      prtOrganizationCD,
      addr,
      validStartDT,
      validEndDT,
      operationDate,
      userID
    } = this.formAddCus.value;

    if (custCD === '') {
      this.messageValidADD = 'Code require not blank !';
      return;
    }

    if (validStartDT === '') {
      this.messageValidADD = 'Valid Start Date require not blank';
      return;
    }

    if (validEndDT === '') {
      this.messageValidADD = 'Valid End Date require not blank';
      return;
    }


    if (validStartDT && validStartDT.length > 10) {
      this.messageValidADD = 'Valid Start Date wrong!';
      return;
    }

    if (validEndDT && validEndDT.length > 10) {
      this.messageValidADD = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(validStartDT, 'yyyy/MM/dd') > this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidADD = 'Valid Start Date wrong , Valid Start Date can not bigger than Today!';
      return;
    }

    if (this.datePipe.transform(validEndDT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidADD = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }
    this.isWaitingRegister = true;
    const form: IFormCreateUpdateCus = {
      classFication: classFication ? classFication : '10',
      custCD,
      custNM,
      custNMENG,
      custBranchNM,
      custBranchNM_EN,
      coRgstNo,
      industryCD,
      prtOrganizationClass,
      prtOrganizationCD,
      addr,
      validStartDT: this.datePipe.transform(validStartDT, 'yyyy/MM/dd'),
      validEndDT: this.datePipe.transform(validEndDT, 'yyyy/MM/dd'),
      userID: this.currentUserName
    };
    console.log(form);
    this.cusService.addCustomer(form).subscribe(
      result => {
        this.isWaitingRegister = false;
        console.log(result);
        this.formAddCus.reset('');
        this.messageValidADD = 'Add Customer Successful';
      }, e => {
        this.isWaitingRegister = false;
        this.messageValidADD = e.message;
      }
    );
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, cust: ICustInfo) {
    this.messageValidUpdate = '';
    console.log(cust);
    this.custInfo = cust;
    openModalUpdate.click();
    this.formUpdateCus.patchValue({
      CUST_GB: cust.CUST_GB,
      CUST_CD: cust.CUST_CD,
      CUST_NM: cust.CUST_NM,
      CUST_NM_ENG: cust.CUST_NM_ENG,
      BRANCH_NM: cust.BRANCH_NM,
      BRANCH_NM_ENG: cust.BRANCH_NM_ENG,
      CO_RGST_NO: cust.CO_RGST_NO,
      BIZ_CG_CD: cust.BIZ_CG_CD,
      PRT_CUST_GB: cust.PRT_CUST_GB,
      PRT_CUST_CD: cust.PRT_CUST_CD,
      ADDR: cust.ADDR,
      VALID_START_DT: cust.VALID_START_DT,
      VALID_END_DT: cust.VALID_END_DT,
      SYS_DTIM: cust.SYS_DTIM,
      WORK_ID: cust.WORK_ID,
      STATUS: cust.STATUS
    });
  }

  getDataToInputSearch(closeOrnName: HTMLButtonElement, cust: ICustInfo) {
   this.formSearchCus.patchValue({
     custClassicfication: cust.CUST_GB,
     cusCd: cust.CUST_CD ? cust.CUST_CD : '',
     custNm: cust.CUST_NM_ENG ? cust.CUST_NM_ENG : ''
   });
    console.log(cust);
    closeOrnName.click();
  }

  updateCusInfo() {
    const {
      CUST_GB,
      CUST_CD,
      CUST_NM,
      CUST_NM_ENG,
      BRANCH_NM,
      BRANCH_NM_ENG,
      CO_RGST_NO,
      BIZ_CG_CD,
      PRT_CUST_GB,
      PRT_CUST_CD,
      ADDR,
      VALID_START_DT,
      VALID_END_DT,
      SYS_DTIM,
      WORK_ID,
      STATUS
    } = this.formUpdateCus.value;
    this.messageValidUpdate = '';
    if (_.isEqual(this.formUpdateCus.value, this.custInfo)) {
      this.messageValidUpdate = 'Nothing Change!';
      return;
    }

    if (VALID_START_DT && VALID_START_DT.length != 10) {
      this.messageValidUpdate = 'Valid Start Date wrong!';
      return;
    }

    if (VALID_END_DT && VALID_END_DT.length != 10) {
      this.messageValidUpdate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd') && STATUS == 1) {
      this.messageValidUpdate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }
    this.isWaitingUpdate = true;
    const form: IFormCreateUpdateCus = {
      classFication: CUST_GB,
      custCD: CUST_CD,
      custNM: CUST_NM,
      custNMENG: CUST_NM_ENG,
      custBranchNM: BRANCH_NM,
      custBranchNM_EN: BRANCH_NM_ENG,
      coRgstNo: CO_RGST_NO,
      industryCD: BIZ_CG_CD,
      prtOrganizationClass: PRT_CUST_GB,
      prtOrganizationCD: PRT_CUST_CD,
      addr: ADDR,
      validStartDT: this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd'),
      validEndDT: this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd'),
      status: STATUS
  };
    console.log(form);
    this.cusService.updateCustomer(form).subscribe(
      result => {
        this.isWaitingUpdate = false;
        if (result.rowsAffected == 1 ) {
          this.messageValidUpdate = 'Updated customer information';
          this.searchInfoCustomer();
          this.custInfo = this.formUpdateCus.value;
        }
      }, err => {
        this.isWaitingUpdate = false;
        if (err.status == 501) {
          this.messageValidUpdate = "Please set all Contract have relation with this Customer expire first!";
        } else {
          this.messageValidUpdate = err.message
        }
      }
    );
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  setTimeSys() {
    this.timeNow = this.datePipe.transform(new Date(), 'yyyy/MM/dd-hh:mm:ss');
    this.messageValidADD = '';
  }
}
