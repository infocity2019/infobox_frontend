import {ICustInfo} from './ICustInfo';

export interface IcustInfoArray {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    ICustInfo
  ];
}
