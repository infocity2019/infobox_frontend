export interface IFormCreateUpdateCus {
  classFication?: string;
  custCD?: string;
  custNM?: string;
  custNMENG?: string;
  custBranchNM?: string;
  custBranchNM_EN?: string;
  coRgstNo?: string;
  industryCD?: string;
  prtOrganizationClass?: string;
  prtOrganizationCD?: string;
  addr?: string;
  validStartDT?: string;
  validEndDT?: string;
  operationDate?: string;
  userID?: string;
  status?: number;
}
