import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {IFormSearchCust} from './IFormSearchCust';
import {ICustInfo} from './ICustInfo';
import {IFormCreateUpdateCus} from './IFormCreateUpdateCus';
import {IcustInfoArray} from './IcustInfoArray';

@Injectable({
  providedIn: 'root'
})
export class ManageCustomerService {
  private cusApi = this.httpG.make('/customer');
  constructor(private httpG: HttpServiceGenerator) { }

  getInfoCus2(form: IFormSearchCust): Observable<ICustInfo[]> {
    return this.cusApi.Get<ICustInfo[]>('getCustInfo', form).pipe
    (tap(result => {
      console.log(result);
    }, e => {
      console.log(e);
    }));
  }

  getInfoCus(form: IFormSearchCust): Observable<IcustInfoArray> {
    return this.cusApi.Post<IcustInfoArray>('getCustInfo', {} ,form).pipe
    (tap(result => {
      console.log(result);
    }, e => {
      console.log(e);
    }));
  }

  addCustomer(form: IFormCreateUpdateCus): Observable<any> {
    return this.cusApi.Post<any>('addCust', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  updateCustomer(form: IFormCreateUpdateCus): Observable<any> {
    return this.cusApi.Put<any>('editCust', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
