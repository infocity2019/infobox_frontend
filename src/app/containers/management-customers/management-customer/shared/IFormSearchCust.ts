export interface IFormSearchCust {
  custClassicfication?: string;
  cusCd?: string;
  custNm?: string;
  status?: number[];
  currentLocation: any;
  limitRow: any;
}
