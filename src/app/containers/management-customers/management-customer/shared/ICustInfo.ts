export interface ICustInfo {
  CUST_GB?: string;
  CUST_CD?: string;
  CUST_NM?: string;
  CUST_NM_ENG?: string;
  BRANCH_NM?: string;
  BRANCH_NM_ENG?: string;
  CO_RGST_NO?: string;
  BIZ_CG_CD?: string;
  PRT_CUST_GB?: string;
  PRT_CUST_CD?: string;
  ADDR?: string;
  VALID_START_DT?: string;
  VALID_END_DT?: string;
  SYS_DTIM?: string;
  WORK_ID?: string;
  STATUS?: number;
}
