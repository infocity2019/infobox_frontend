import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {IFormSearchCust} from '../../management-customer/shared/IFormSearchCust';
import {Observable} from 'rxjs';
import {ICustInfo} from '../../management-customer/shared/ICustInfo';
import {tap} from 'rxjs/operators';
import {IFormSearchContract} from './IFormSearchContract';
import {IContract} from './icontract';
import {IFormCreateUpdateContract} from './iform-create-update-contract';
import {IFormSeachProduct} from './IFormSeachProduct';
import {IProduct} from './iproduct';
import {IContractPagination} from './IContractPagination';
import {ISearchProductPagination} from './ISearchProductPagination';

@Injectable({
  providedIn: 'root'
})
export class ManagementContractService {
  private contractAPI = this.httpG.make('/contract');
  constructor(private httpG: HttpServiceGenerator) { }

  getContract(form: IFormSearchContract): Observable<IContractPagination> {
    return this.contractAPI.Post<IContractPagination>('getContract', {} , form).pipe
    (tap(result => {
      console.log(result);
    }, e => {
      console.log(e);
    }));
  }

  getProductCode(form: IFormSeachProduct): Observable<ISearchProductPagination> {
    return this.contractAPI.Get<ISearchProductPagination>('getProduct', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  createContract(form: IFormCreateUpdateContract): Observable<any> {
    return this.contractAPI.Post<any>( 'insertContract', {} , form).pipe
    (tap( result => {
      console.log(result);
    }, e => {
      console.log(e);
    }));
  }

  checkExistContract(form: IFormCreateUpdateContract): Observable<any> {
    return this.contractAPI.Post<any>( 'checkExistContract', {} , form).pipe
    (tap( result => {
      console.log(result);
    }, e => {
      console.log(e);
    }));
  }

  updateContract(form: IFormCreateUpdateContract): Observable<any> {
    return this.contractAPI.Put<any>( 'updateContract', {} , form).pipe
    (tap( result => {
      console.log(result);
    }, e => {
      console.log(e);
    }));
  }
}
