export interface ISearchContract {
  organClassifi?: string;
  organCd?: string;
  organNM?: string;
  productCode?: string;
  currentLocation?: any;
  limitRow?: any;
}
