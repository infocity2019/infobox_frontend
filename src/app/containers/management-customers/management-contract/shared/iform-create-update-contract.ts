export interface IFormCreateUpdateContract {
  cusGB?: string;
  custCD?: string;
  gdsCD?: string;
  validStartDt?: string;
  validEndDt?: string;
  sysDTim?: string;
  workID?: string;
  status?: number;
}
