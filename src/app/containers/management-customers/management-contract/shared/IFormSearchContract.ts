export interface IFormSearchContract {
  organClassifi?: string;
  organCd?: string;
  organNM?: string;
  productCode?: string;
  status?: number[];
  currentLocation?: number;
  limitRow?: number;
}
