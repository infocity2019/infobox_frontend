import {IFormSeachProduct} from './IFormSeachProduct';
import {IProduct} from './iproduct';
export interface ISearchProductPagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IProduct
  ];
}
