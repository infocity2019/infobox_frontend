import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from '../../../../shared/service';
import {ISearchContract} from './ISearchContract';
import {Observable} from 'rxjs';
import {IContract} from './icontract';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContractService {
  private cusApi = this.httpG.make('/contract');
  constructor(private httpG: HttpServiceGenerator) { }

  getInfoContract(form: ISearchContract): Observable<IContract[]> {
    return this.cusApi.Get<IContract[]>('/getContract', form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }
}
