import {IContract} from './icontract';

export interface IContractPagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IContract
  ];
}
