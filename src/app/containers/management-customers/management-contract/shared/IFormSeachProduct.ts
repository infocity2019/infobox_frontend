export interface IFormSeachProduct {
  productCode?: string;
  productNM?: string;
  currentLocation: number;
  limitRow: number;
}
