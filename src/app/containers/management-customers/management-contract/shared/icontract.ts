export interface IContract {
  CUST_GB?: string;
  CUST_CODE?: string;
  PRODUCT_CODE?: string;
  CUST_NM_ENG?: string;
  BRANCH_NM?: string;
  CODE_NM?: string ;
  VALID_START_DT?: string;
  VALID_END_DT?: string;
  SYS_DTIM?: string;
  WORK_ID?: string;
  STATUS?: number;
}
