import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {IFormSearchCust} from '../management-customer/shared/IFormSearchCust';
import {ManagementContractService} from './shared/management-contract.service';
import {IFormSearchContract} from './shared/IFormSearchContract';
import {IContract} from './shared/icontract';
import {IFormCreateUpdateCus} from '../management-customer/shared/IFormCreateUpdateCus';
import {IFormCreateUpdateContract} from './shared/iform-create-update-contract';
import {ManageCustomerService} from '../management-customer/shared/manageCustomer.service';
import {DatePipe} from '@angular/common';
import {IFormSeachProduct} from './shared/IFormSeachProduct';
import {IProduct} from './shared/iproduct';
import {ICustInfo} from '../management-customer/shared/ICustInfo';
import {valueReferenceToExpression} from '@angular/compiler-cli/src/ngtsc/annotations/src/util';
import {log} from 'util';
import {PagerService} from '../../../shared/service/pager.service';
import {IContractPagination} from './shared/IContractPagination';
import {ISearchProductPagination} from './shared/ISearchProductPagination';
import {IcustInfoArray} from '../management-customer/shared/IcustInfoArray';
import * as _ from 'lodash';
import {CheckRoleService} from '../../../shared/service/check-role.service';

@Component({
  selector: 'app-management-contract',
  templateUrl: './management-contract.component.html',
  styleUrls: ['./management-contract.component.scss']
})
export class ManagementContractComponent implements OnInit {
  formSearchContract = new FormGroup({
    organClassifi: new FormControl(''),
    organCd: new FormControl(''),
    organNM: new FormControl(''),
    productCode: new FormControl(''),
    status: new FormControl(''),
  });
  formCreateContract = new FormGroup({
    cusGB: new FormControl(''),
    custCD: new FormControl(''),
    gdsCD: new FormControl(''),
    productName: new FormControl({value: '', disabled: true}),
    OrgName: new FormControl({value: '', disabled: true}),
    branchName: new FormControl({value: '', disabled: true}),
    validStartDt: new FormControl(''),
    validEndDt: new FormControl(''),
    sysDTim: new FormControl(''),
    workID: new FormControl(''),
  });


  formUpdateContract = new FormGroup({
    CUST_GB: new FormControl(''),
    CUST_CODE: new FormControl(''),
    PRODUCT_CODE: new FormControl(''),
    CUST_NM_ENG: new FormControl(''),
    BRANCH_NM: new FormControl(''),
    CODE_NM: new FormControl(''),
    VALID_START_DT: new FormControl(''),
    VALID_END_DT: new FormControl(''),
    SYS_DTIM: new FormControl(''),
    WORK_ID: new FormControl(''),
    STATUS: new FormControl(''),
  });

  formSearchProductCode = new FormGroup({
    productCode: new FormControl(''),
    productNM: new FormControl('')
  });

  formSearchOrgModal = new FormGroup({
    custClassicfication: new FormControl(''),
    cusCd: new FormControl(''),
    custNm: new FormControl(''),
  });

  hiddenNext: boolean;
  hiddenPre: boolean;
  currentLocation = 0;
  limitRow = 10;
  timeQuery: string;
  mesQuery: string;
  listContract: IContractPagination;
  messageValidCreate = '';
  listProduct: ISearchProductPagination;
  mesSearchOrg: string;
  listOrg: IcustInfoArray;
  contract: IContract;
  messageValidUpdate: string;
  isLooking: boolean;
  isLookingProduct: boolean;
  isLookingOrg: boolean;
  isWaitingRegister: boolean;
  isWaitingUpdate: boolean;
  pagerOrg: any = {};
  pagerContract: any = {};
  pagerProduct: any = {};
  productCodeModal: any;
  productNMModal: any;
  organClassifiContract: any;
  organCdContract: any;
  organNMContract: any;
  productCodeContract: any;
  custClassicficationModal: any;
  cusCdModal: any;
  custNmModal: any;
  timeNow: string;
  currentUserName: string;
  statusContract: any;
  roleAccount: string;
  checkRoleAccount: { isAccessCaptcha: boolean; isRoleAdmin: boolean; isRoleUser: boolean };
  statusArr: number[] = [];
  constructor(private contractService: ManagementContractService,
              private customerService: ManageCustomerService,
              private cusService: ManageCustomerService,
              private datePipe: DatePipe,
              private checkRole: CheckRoleService,
              private pagerService: PagerService) {
    this.currentUserName = localStorage.getItem('UserName');
    this.checkRoleAccount = this.checkRole.checkRoleAccount();
  }

  ngOnInit() {
  }

  resetValueInputSearchOrgName() {
    this.formSearchContract.patchValue({
      organNM: ''
    })
  }

  setPageSearchContract(page: number) {
    console.log('>>> contract');
    if (page < 1 || page > this.pagerContract.totalPages) {
      return;
    }
    if (this.listContract) {
      // get pager object from service
      this.pagerContract = this.pagerService.getPager(this.listContract.count[0].TOTAL, page);
    }
  }
  setPageSearchProduct(page: number) {
    console.log('>>> product');
    if (page < 1 || page > this.pagerProduct.totalPages) {
      return;
    }
    if (this.listProduct) {
      // get pager object from service
      this.pagerProduct = this.pagerService.getPager(this.listProduct.count[0].TOTAL, page);
    }
  }

  openDetailForm(contract: IContract, openModalUpdate: HTMLButtonElement) {
    this.contract = contract;
    openModalUpdate.click();
    this.messageValidUpdate = '';
    this.formUpdateContract.patchValue({
      CUST_GB: this.contract.CUST_GB,
      CUST_CODE: this.contract.CUST_CODE,
      PRODUCT_CODE: this.contract.PRODUCT_CODE,
      CUST_NM_ENG: this.contract.CUST_NM_ENG,
      BRANCH_NM: this.contract.BRANCH_NM,
      CODE_NM: this.contract.CODE_NM,
      VALID_START_DT: this.contract.VALID_START_DT,
      VALID_END_DT: this.contract.VALID_END_DT,
      SYS_DTIM: this.contract.SYS_DTIM,
      WORK_ID: this.contract.WORK_ID,
      STATUS: this.contract.STATUS,
    });
  }

  productSearch() {
    this.isLookingProduct = true;
    const {productCode, productNM} = this.formSearchProductCode.value;
    const form: IFormSeachProduct = {
      productCode,
      productNM,
      currentLocation:  this.currentLocation ,
      limitRow: this.limitRow
    };
    console.log(form);
    this.contractService.getProductCode(form).subscribe(
      result => {
        this.isLookingProduct = false;
        if (result.rowRs[0]) {
          this.productCodeModal = productCode;
          this.productNMModal = productNM;
          this.listProduct = result;
          this.setPageSearchProduct(1);
        } else {
          this.listProduct = null;
        }
      }, error => {
        this.isLookingProduct = false;
        console.log(error);
      }
    );
  }

  paginationProduct(location: number) {
    console.log(location);
    const form: IFormSeachProduct = {
      productCode: this.productCodeModal,
      productNM: this.productNMModal,
      currentLocation: location * this.limitRow ,
      limitRow: this.limitRow
    };
    console.log(form);
    this.contractService.getProductCode(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listProduct = result;
        } else {
          this.listProduct = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  searchContract() {
    this.statusArr = [];
    this.isLooking = true;
    let {organClassifi, organCd, organNM, productCode, status} = this.formSearchContract.value;
    if (status == '' || status == 1) {
      this.statusArr.push(1);
    } else if (status == 3) {
      this.statusArr = null;
    } else if (status == 2) {
      this.statusArr.push(0);
    }
    const searchForm: IFormSearchContract = {
      organClassifi,
      organCd,
      organNM,
      productCode,
      status: this.statusArr,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.contractService.getContract(searchForm).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.listContract = result;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.organClassifiContract = organClassifi;
          this.organCdContract = organCd;
          this.organNMContract = organNM;
          this.productCodeContract = productCode;
          this.statusContract = this.statusArr;
          this.setPageSearchContract(1);
        } else {
          this.mesQuery = 'Not Found!';
          this.getDate();
          this.listContract = null;
        }
      }, error => {
        console.log(error);
        this.isLooking = false;
      }
    );
  }

  paginationContract(location: number) {
    const searchForm: IFormSearchContract = {
      organClassifi: this.organClassifiContract,
      organCd: this.organCdContract,
      organNM: this.organNMContract,
      productCode: this.productCodeContract,
      status: this.statusContract,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(searchForm);
    this.contractService.getContract(searchForm).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listContract = result;
        } else {
          this.listContract = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  resetValueFormCreateContract() {
    this.formCreateContract.reset('');
    this.messageValidCreate = '';
    this.timeNow = this.datePipe.transform(new Date(), 'yyyy/MM/dd-hh:mm:ss');
  }

  createNewContract(closeModalAlertExistContract: HTMLButtonElement) {
    this.messageValidCreate = '';
    closeModalAlertExistContract.click();
    const {
      cusGB,
      custCD,
      gdsCD,
      validStartDt,
      validEndDt,
      sysDTim,
      workID
    } = this.formCreateContract.value;

    if (cusGB === '' || cusGB === null) {
      this.messageValidCreate = 'Organization Classification require not blank !';
      return;
    }

    if (custCD === '' || custCD === null) {
      this.messageValidCreate = 'Organization Code require not blank !';
      return;
    }

    if (gdsCD === '' || gdsCD === null) {
      this.messageValidCreate = 'Product Code require not blank !';
      return;
    }

    if (validStartDt === '' || validStartDt === null) {
      this.messageValidCreate = 'Valid Start Date require not blank !';
      return;
    }

    if (validStartDt && validStartDt.length > 10) {
      this.messageValidCreate = 'Valid Start Date wrong!';
      return;
    }

    if (validEndDt && validEndDt.length > 10) {
      this.messageValidCreate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(validStartDt, 'yyyy/MM/dd') >= this.datePipe.transform(validEndDt, 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid Start Date wrong , Valid Start Date can not bigger than Valid End Date or the same!';
      return;
    }

    if (this.datePipe.transform(validEndDt, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }
    this.isWaitingRegister = true;
    const form: IFormCreateUpdateContract = {
      cusGB,
      custCD,
      gdsCD,
      validStartDt: this.datePipe.transform(validStartDt, 'yyyy/MM/dd'),
      validEndDt: this.datePipe.transform(validEndDt, 'yyyy/MM/dd'),
      workID: this.currentUserName,
    };
    console.log(form);
    this.contractService.createContract(form).subscribe(
      result => {
        this.isWaitingRegister = false;
        this.formCreateContract.reset('');
        this.messageValidCreate = 'Create Contract Successful.';
      }, e => {
        this.isWaitingRegister = false;
        this.messageValidCreate = e.message;
      }
    );
  }

  checkExistContract(buttonOpenAlertExistContract: HTMLButtonElement, closeModalAlertExistContract: HTMLButtonElement) {
    this.messageValidCreate = '';
    const {
      cusGB,
      custCD,
      gdsCD,
      validStartDt,
      validEndDt,
      sysDTim,
      workID
    } = this.formCreateContract.value;

    if (cusGB === '' || cusGB === null) {
      this.messageValidCreate = 'Organization Classification require not blank !';
      return;
    }

    if (custCD === '' || custCD === null) {
      this.messageValidCreate = 'Organization Code require not blank !';
      return;
    }

    if (gdsCD === '' || gdsCD === null) {
      this.messageValidCreate = 'Product Code require not blank !';
      return;
    }

    if (validStartDt === '' || validStartDt === null) {
      this.messageValidCreate = 'Valid Start Date require not blank !';
      return;
    }

    if (validStartDt && validStartDt.length > 10) {
      this.messageValidCreate = 'Valid Start Date wrong!';
      return;
    }

    if (validEndDt && validEndDt.length > 10) {
      this.messageValidCreate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(validStartDt, 'yyyy/MM/dd') >= this.datePipe.transform(validEndDt, 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid Start Date wrong , Valid Start Date can not bigger than Valid End Date or the same!';
      return;
    }

    if (this.datePipe.transform(validEndDt, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd')) {
      this.messageValidCreate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }
    this.isWaitingRegister = true;
    const form: IFormCreateUpdateContract = {
      cusGB,
      custCD,
      gdsCD,
      validStartDt: this.datePipe.transform(validStartDt, 'yyyy/MM/dd'),
      validEndDt: this.datePipe.transform(validEndDt, 'yyyy/MM/dd'),
      workID: this.currentUserName,
    };
    console.log(form);
    this.contractService.checkExistContract(form).subscribe(
      result => {
        this.isWaitingRegister = false;
        if (result.message) {
          buttonOpenAlertExistContract.click();
        } else if (result.saveContract) {
          this.createNewContract(closeModalAlertExistContract);
        }
      }, e => {
        this.isWaitingRegister = false;
        this.messageValidCreate = e.message;
      }
    );
  }

  updateContract() {
    const {
      CUST_GB,
      CUST_CODE,
      PRODUCT_CODE,
      CUST_NM_ENG,
      BRANCH_NM,
      CODE_NM,
      VALID_START_DT,
      VALID_END_DT,
      SYS_DTIM,
      WORK_ID,
      STATUS,
    } = this.formUpdateContract.value;
    this.messageValidUpdate = '';

    if (_.isEqual(this.formUpdateContract.value, this.contract)) {
      console.log(_.isEqual(this.formUpdateContract.value, this.contract));
      this.messageValidUpdate = 'Nothing Change!';
      return;
    }

    if (VALID_START_DT && VALID_START_DT.length > 10) {
      this.messageValidUpdate = 'Valid Start Date wrong!';
      return;
    }

    if (VALID_END_DT && VALID_END_DT.length > 10) {
      this.messageValidUpdate = 'Valid End Date wrong!';
      return;
    }

    if (this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd') <= this.datePipe.transform(new Date(), 'yyyy/MM/dd') && STATUS == 1) {
      this.messageValidUpdate = 'Valid End Date wrong , Valid End Date must bigger than Today!';
      return;
    }
    this.isWaitingUpdate = true;
    const form: IFormCreateUpdateContract = {
      gdsCD: PRODUCT_CODE,
      cusGB: CUST_GB,
      custCD: CUST_CODE,
      validEndDt: this.datePipe.transform(VALID_END_DT, 'yyyy/MM/dd'),
      validStartDt: this.datePipe.transform(VALID_START_DT, 'yyyy/MM/dd'),
      status: STATUS
    };
    console.log(form);
    this.contractService.updateContract(form).subscribe(
      result => {
        this.isWaitingUpdate = false;
        if (result.rowsAffected == 1) {
          this.messageValidUpdate = 'Updated Contract.';
          this.contract = this.formUpdateContract.value;
          this.searchContract();
        }
      }, err => {
        this.isWaitingUpdate = false;
        this.messageValidUpdate = err.message;
      }
    );
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  getValueProductCode(product: IProduct, closeSearchProductCodeModal: HTMLButtonElement) {
    if (product) {
      this.formSearchContract.patchValue({
        productCode: product.CODE
      });
    }
    console.log(product);
    closeSearchProductCodeModal.click();
  }

  setPageSearchCustModal(page: number) {
    if (page < 1 || page > this.pagerOrg.totalPages) {
      return;
    }
    if (this.listOrg) {
      // get pager object from service
      this.pagerOrg = this.pagerService.getPager(this.listOrg.count[0].TOTAL, page);
    }
  }

  searchOrgByNameAndClass() {
    this.isLookingOrg = true;
    const {custClassicfication, cusCd , custNm} = this.formSearchOrgModal.value;
    const form: IFormSearchCust = {
      custClassicfication,
      cusCd,
      custNm,
      status: [1] ,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        this.isLookingOrg = false;
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.custClassicficationModal = custClassicfication;
          this.cusCdModal = cusCd;
          this.custNmModal = custNm;
          this.listOrg = result;
          this.setPageSearchCustModal(1);
        } else {
          this.listOrg = null;
        }
      }, error => {
        this.isLookingOrg = false;
      }
    );
  }

  paginationCustModal(location: number) {
    const form: IFormSearchCust = {
      custClassicfication: this.custClassicficationModal,
      cusCd: this.cusCdModal,
      custNm: this.custNmModal,
      status: [1] ,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    this.cusService.getInfoCus(form).subscribe(
      result => {
        if (result.rowRs[0]) {
          this.listOrg = result;
        } else {
          this.listOrg = null;
        }
      }, error => {
        console.log(error);
      }
    );
  }

  getDataToInputSearch(closeOrnName: HTMLButtonElement, cust: ICustInfo) {
    this.formSearchContract.patchValue({
      organClassifi: cust.CUST_GB,
      organCd: cust.CUST_CD ? cust.CUST_CD : '',
      organNM: cust.CUST_NM_ENG ? cust.CUST_NM_ENG : ''
    });
    console.log(cust);
    closeOrnName.click();
  }

  getValueProductCodeForInputRegister(product: IProduct, closeSearchProductCodeModalRegister: HTMLButtonElement) {
    this.formCreateContract.patchValue({
      gdsCD: product.CODE,
      productName: product.CODE_NM
    });
    closeSearchProductCodeModalRegister.click();
  }

  getValueOrgToInputRegister(closeOrnNameModalRegister: HTMLButtonElement, cust: ICustInfo) {
    this.formCreateContract.patchValue({
      cusGB: cust.CUST_GB,
      custCD: cust.CUST_CD,
      OrgName: cust.CUST_NM_ENG ? cust.CUST_NM_ENG : '',
      branchName: cust.BRANCH_NM ? cust.BRANCH_NM : ''
    });
    closeOrnNameModalRegister.click();
  }

  getValueProductCodeForInputUpdate(product: IProduct, closeSearchProductCodeModalUpdate: HTMLButtonElement) {
    this.contract.PRODUCT_CODE = product.CODE;
    closeSearchProductCodeModalUpdate.click();
  }

  getValueOrgToInputUpdate(closeOrnNameModalUpdate: HTMLButtonElement, cust: ICustInfo) {
    this.contract.CUST_GB = cust.CUST_GB;
    this.contract.CUST_CODE = cust.CUST_CD;
    closeOrnNameModalUpdate.click();
  }
}
