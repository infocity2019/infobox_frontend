import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementContractComponent } from './management-contract.component';

describe('ManagementContractComponent', () => {
  let component: ManagementContractComponent;
  let fixture: ComponentFixture<ManagementContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
