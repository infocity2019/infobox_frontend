import { Component, OnInit } from '@angular/core';
import {ManualCTB003Service} from './shared/manual-ctb003.service';
import {IManualCTB003Res} from './shared/imanual-ctb003-res';
import {DatePipe} from '@angular/common';
import {IManualCTRequest} from '../manual-captcha-b002/shared/IManualCTRequest';
import {IB003Res} from './shared/ib003-res';
import {IB003Req} from './shared/ib003-req';
import {FormControl, FormGroup} from '@angular/forms';
import {PagerService} from '../../shared/service/pager.service';
import {IManualCTB003Pagination} from './shared/IManualCTB003Pagination';
import {IFormCICRequest} from '../management-internal/management-cic-rp-view/shared/iform-cicrequest';
import {IFormRequestGetDataForB003} from './shared/IFormRequestGetDataForB003';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-manual-captcha-b003',
  templateUrl: './manual-captcha-b003.component.html',
  styleUrls: ['./manual-captcha-b003.component.css']
})
export class ManualCaptchaB003Component implements OnInit {
  listB003: IManualCTB003Pagination;
  timeQuery: string;
  mesQuery: string;
  B003: IManualCTB003Res;
  messCaptcha: string;
  responseB003: IB003Res;
  inputCaptcha: string;
  isSubmitting: boolean;
  cmtID: string;
  processCaptcha: Subscription;
  listB003AfterSendCaptcha: any;
  listNiceSSID: string[] = [];
  dataCIC: object[] = [];
  formSearchB0003 = new FormGroup({
    niceSsID: new FormControl(''),
  });
  isLooking: boolean;
  pagerB0003: any = {};
  currentLocation = 0;
  limitRow = 10;
  niceSsIDPagination: any;
  messCaptchaInput: string;
  constructor(private manualCTB003Service: ManualCTB003Service,
              private datePipe: DatePipe,
              private pagerService: PagerService) { }

  ngOnInit(): void {
  }

  closeProcessCaptcha(closeModalB0003: HTMLButtonElement, closeModalConfirm: HTMLButtonElement) {
    if (this.processCaptcha) {
      this.processCaptcha.unsubscribe();
    }
    this.searchB0003();
    closeModalB0003.click();
    closeModalConfirm.click();
  }

  setPageSearchB0003(page: number) {
    if (page < 1 || page > this.pagerB0003.totalPages) {
      return;
    }
    if (this.listB003) {
      // get pager object from service
      this.pagerB0003 = this.pagerService.getPager(this.listB003.count[0].TOTAL, page);
    }
  }

  searchB0003() {
    this.isLooking = true;
    const {
      niceSsID
    } = this.formSearchB0003.value;

    const form: IFormRequestGetDataForB003 = {
      niceSsID,
      currentLocation: this.currentLocation,
      limitRow: this.limitRow
    };
    console.log(form);
    this.manualCTB003Service.getAllB003(form).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.niceSsIDPagination = niceSsID;
          this.mesQuery = 'Successful!';
          this.getDate();
          this.listB003 = result;
          this.setPageSearchB0003(1);
        } else {
          this.mesQuery = 'Not Found!';
          this.getDate();
          this.listB003 = null ;
        }
      }, error => {
        this.isLooking = false;
      }
    );
  }

  paginationB0003(location: number) {
    const form: IFormRequestGetDataForB003 = {
      niceSsID: this.niceSsIDPagination,
      currentLocation: location * this.limitRow,
      limitRow: this.limitRow
    };
    console.log(form);
    this.manualCTB003Service.getAllB003(form).subscribe(
      result => {
        this.isLooking = false;
        if (result.rowRs[0]) {
          this.listB003 = result;
        } else {
          this.listB003 = null ;
        }
      }
    );
  }

  getDate() {
    const now = new Date();
    this.timeQuery = this.datePipe.transform(now, 'yyyy-MM-dd hh:mm:ss');
  }

  openDetailForm(openModalUpdate: HTMLButtonElement, b003: IManualCTB003Res) {
    openModalUpdate.click();
    this.B003 = b003;
    this.inputCaptcha = '';
    this.messCaptcha = '';
    this.messCaptchaInput = '';
    this.responseB003 = null;
    this.listB003AfterSendCaptcha = null;
    this.isSubmitting = false;
  }

  B003SendData() {
    this.listNiceSSID = [];
    this.dataCIC = [];
    this.messCaptcha = '';
    this.messCaptchaInput = '';
    if (this.B003) {
      if (this.B003.NATL_ID) {
        this.cmtID = this.B003.NATL_ID;
      } else if (this.B003.OLD_NATL_ID) {
        this.cmtID = this.B003.OLD_NATL_ID;
      } else if (this.B003.PSPT_NO) {
        this.cmtID = this.B003.PSPT_NO;
      } else {
        console.log('cmt null');
        return;
      }
      if (!this.inputCaptcha && this.responseB003) {
        return this.messCaptchaInput = 'Captcha can not blank!';
      }
      this.isSubmitting = true;
      this.listNiceSSID.push(this.B003.NICE_SSIN_ID);
      const obj = {
        NICESESSIONKEY: this.B003.NICE_SSIN_ID,
        CICID: this.B003.S_CIC_NO,
        INQ_DTIM: this.B003.INQ_DTIM
      };
      this.dataCIC.push(obj);
      const form: IB003Req = {
        appCd: 'INFOBOX',
        iftUrl: 'http://www.infotech3.co.kr/ift/deploy/',
        orgCd: 'cic.vn',
        svcCd: 'B0003',
        dispNm: 'cic.org.vn',
        cicNo: '',
        taxNo: '',
        userId: this.B003.S_USER_ID,
        userPw: atob(this.B003.S_USER_PW),
        customerType: '2',
        cmtNo: '',
        step_useYn: 'Y',
        step_input: this.inputCaptcha ? this.inputCaptcha : '',
        step_data: this.responseB003 ? this.responseB003.dataStep : '',
        reportType: '06',
        voteNo: '111',
        niceSessionKey: this.listNiceSSID,
        inqDt1: this.B003.INQ_DTIM,
        inqDt2: this.B003.INQ_DTIM,
        reportCicNo: this.B003.S_CIC_NO,
        sendTime: '',
        dataCic: this.dataCIC
      };
      console.log(form);
      this.processCaptcha = this.manualCTB003Service.SendDataToB003(form).subscribe(
        result => {
          this.isSubmitting = false;
          if (result.imgBase64) {
            this.responseB003 = result;
            this.inputCaptcha = '';
          } else if (result.outJson.errMsg) {
            this.messCaptcha = result.outJson.errMsg;
          } else if (result.outJson.outB0002.errMsg) {
            this.messCaptcha = result.outJson.outB0002.errMsg;
          } else if (result.outJson.outB0003.errMsg) {
            this.messCaptcha = result.outJson.outB0002.errMsg;
          } else if (result.outJson.outB0003.list) {
            this.listB003AfterSendCaptcha = result.outJson.outB0003.list;
          } else if (result.outJson.outB0003.list.length == 0) {
            this.messCaptcha = 'No result found!';
          }
        } , error => {
          this.isSubmitting = false;
          this.messCaptcha = error.message;
        }
      );
    }
  }

  checkToCloseModalB0003(closeModalB0003: HTMLButtonElement, openModalConfirmCloseB0003: HTMLButtonElement) {
    if (this.isSubmitting || this.responseB003 || this.listB003AfterSendCaptcha) {
      openModalConfirmCloseB0003.click();
    } else {
      closeModalB0003.click();
    }
  }
}
