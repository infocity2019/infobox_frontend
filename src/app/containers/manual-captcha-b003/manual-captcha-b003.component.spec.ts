import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualCaptchaB003Component } from './manual-captcha-b003.component';

describe('ManualCaptchaB003Component', () => {
  let component: ManualCaptchaB003Component;
  let fixture: ComponentFixture<ManualCaptchaB003Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualCaptchaB003Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualCaptchaB003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
