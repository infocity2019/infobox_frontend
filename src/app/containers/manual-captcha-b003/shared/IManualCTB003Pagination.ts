import {IManualCTB003Res} from './imanual-ctb003-res';

export interface IManualCTB003Pagination {
  count: [
    {
      TOTAL: number;
    }
  ];
  rowRs: [
    IManualCTB003Res
  ];
}
