import { Injectable } from '@angular/core';
import {HttpServiceGenerator} from "../../../shared/service";
import {Observable} from "rxjs";
import {IManualCTB003Res} from "./imanual-ctb003-res";
import {tap} from "rxjs/operators";
import {IB003Req} from "./ib003-req";
import {IB003Res} from "./ib003-res";
import {IManualCTB003Pagination} from './IManualCTB003Pagination';
import {IFormRequestGetDataForB003} from './IFormRequestGetDataForB003';

@Injectable({
  providedIn: 'root'
})
export class ManualCTB003Service {
  private searchB003URL = this.httpG.make('manualCaptcha');
  private internalAPI = this.httpG.makeInternal('internal');
  constructor(private httpG: HttpServiceGenerator) { }

  getAllB003(form: IFormRequestGetDataForB003): Observable<IManualCTB003Pagination> {
    return this.searchB003URL.Get<IManualCTB003Pagination>('getListB003' , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e);
      }
    ));
  }

  SendDataToB003(form: IB003Req): Observable<IB003Res> {
    return this.internalAPI.Post<IB003Res>('cicB0003', {} , form).pipe
    (tap(
      result => {
        console.log(result);
      }, e => {
        console.log(e)
      }
    ))
  }


}
