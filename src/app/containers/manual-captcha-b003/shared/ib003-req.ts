export interface IB003Req {
  appCd: string;
  iftUrl: string;
  orgCd: string;
  svcCd: string;
  dispNm: string;
  userId: string;
  userPw: string;
  customerType: string;
  cicNo?: string;
  taxNo?: string;
  cmtNo: string;
  step_useYn: string;
  step_input?: any;
  step_data?: any;
  reportType: string;
  voteNo: string;
  niceSessionKey: string[];
  inqDt1: string;
  inqDt2: string;
  reportCicNo: string;
  sendTime: string;
  reqStatus?: string;
  dataCic: any;
}
