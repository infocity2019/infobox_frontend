export interface IManualCTB003Res {
  NICE_SSIN_ID: string;
  S_CIC_NO: string;
  INQ_DTIM: string;
  S_USER_ID: string;
  S_USER_PW: string;
  NATL_ID?: string;
  OLD_NATL_ID?: string;
  PSPT_NO?: string;
}
