import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { LoginComponent } from './auth/login/login.component';
import { SessionValidationGuard } from './auth';
import {ManagementCustomerComponent} from './containers/management-customers/management-customer/management-customer.component';
import {ManagementContractComponent} from './containers/management-customers/management-contract/management-contract.component';
import {ManagementCodeComponent} from './containers/management-internal/management-code/management-code.component';

import {ManagementCicRpViewComponent} from './containers/management-internal/management-cic-rp-view/management-cic-rp-view.component';
import {ManagementUserComponent} from './containers/management-system/management-user/management-user.component';
import {ManagementCaptchaAdminComponent} from './containers/management-system/management-captcha-admin/management-captcha-admin.component';
import {ManualCaptchaComponent} from "./containers/manual-captcha-b002/manual-captcha.component";
import {ManualCaptchaB003Component} from "./containers/manual-captcha-b003/manual-captcha-b003.component";
import {ManualCaptchaB001Component} from "./containers/manual-captcha-b001/manual-captcha-b001.component";
import {UserCanActive} from './auth/shared/UserCanActive';
import {AdminCanActive} from './auth/shared/AdminCanActive';
import {CaptchaUserCanActive} from './auth/shared/CaptchaUserCanActive';
import {ManagementChartComponent} from './containers/management-chart/management-chart.component';
import {ManagementLogComponent} from './containers/management-log/management-log.component';
import {FPTBalanceComponent} from './containers/FPTBalance/FPTBalance.component';
import {CountTransactionComponent} from './containers/management-transaction/count-transaction/count-transaction.component';


export const routes: Routes = [
  { path: "auth", loadChildren: "./auth/index#AuthModule" },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [SessionValidationGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'default',
        component: ManagementCustomerComponent,
        canActivate: [UserCanActive]
      },
      {
        path: 'customer/management-customer',
        component: ManagementCustomerComponent,
        canActivate: [UserCanActive]
      },
      {
        path: 'customer/management-contract',
        component: ManagementContractComponent,
        canActivate: [UserCanActive]
      },
      {
        path: 'internal/management-code',
        component: ManagementCodeComponent,
        canActivate: [UserCanActive]
      },
      {
        path: 'internal/management-CICrpView',
        component: ManagementCicRpViewComponent,
        canActivate: [UserCanActive]
      },
      {
        path: 'system/management-user',
        component: ManagementUserComponent,
      },
      {
        path: 'system/management-captcha',
        component: ManagementCaptchaAdminComponent,
        canActivate: [AdminCanActive],
      },
      {
        path: 'manual/manual-captcha-b002',
        component: ManualCaptchaComponent,
        canActivate: [CaptchaUserCanActive],
      },
      {
        path: 'manual/manual-captcha-b003',
        component: ManualCaptchaB003Component,
        canActivate: [CaptchaUserCanActive],
      },
      {
        path: 'manual-captcha-b1003',
        component: ManualCaptchaB001Component,
        canActivate: [CaptchaUserCanActive],
      },
      {
        path: 'monitor/chart',
        component: ManagementChartComponent,
      },
      {
        path: 'monitor/log',
        component: ManagementLogComponent,
      },
      {
        path: 'monitor/FPTBalance',
        component: FPTBalanceComponent,
      },
      {
        path: 'monitor/countTransactions',
        component: CountTransactionComponent
      }
    ]
  },
];

export const appRouting = RouterModule.forRoot(routes, { useHash: true });


@NgModule({
  imports: [appRouting],
  exports: [RouterModule]
})
export class AppRoutingModule { }
