import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  constructor() {
  }

  getMenu(): Array<any> {
    const menu = [
      {
        name: 'Home',
        path: './',
      },
      {
        name: 'Customer',
        path: './customer',
        children: [
          {
            name: 'Management Customer',
            path: './management-customer'
          },
          {
            name: 'Management Contract',
            path: './management-contract'
          }
        ]
      },
      {
        name: 'Internal',
        path: './internal',
        children: [
          {
            name: 'Management Code',
            path: './management-code'
          },
          {
            name: 'Management CIC Report View Link',
            path: './management-CICrpView'
          },
        ]
      },
      {
        name: 'System',
        path: './system',
        children: [
          {
            name: 'Management User',
            path: './management-user'
          },
          {
            name: 'Management CAPTCHA',
            path: './management-captcha'
          }
        ]
      },
      {
        name: 'Manual CAPTCHA',
        path: './manual',
        children: [
          {
            name: 'Manual CAPTCHA B0002',
            path: './manual-captcha-b002'
          },
          {
            name: 'Manual CAPTCHA B1003',
            path: './manual-captcha-b1003'
          },
          {
            name: 'Manual CAPTCHA B0003',
            path: './manual-captcha-b003'
          }
        ]
      }, {
        name: 'Monitor',
        path: './monitor',
        children: [
          {
            name: 'Chart',
            path: './chart'
          },
          {
            name: 'Count Transactions',
            path: './countTransactions'
          },
          {
            name: 'Log Monitoring',
            path: './log'
          },
          {
            name: 'FPT Balance',
            path: './FPTBalance'
          }
        ]
      }
    ];

    return menu;
  }
}

