import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HttpClientModule } from '@angular/common/http';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import routing module
import { AppRoutingModule } from './app.routing';
import { DefaultLayoutModule } from './containers/default-layout/default-layout.module';
import {ReactiveFormsModule} from '@angular/forms';
import { ManagementChartComponent } from './containers/management-chart/management-chart.component';
import { ManagementLogComponent } from './containers/management-log/management-log.component';
import { FPTBalanceComponent } from './containers/FPTBalance/FPTBalance.component';
import {ChartsModule} from 'ng2-charts';
import {MatButtonModule} from '@angular/material';
import { CountTransactionComponent } from './containers/management-transaction/count-transaction/count-transaction.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DefaultLayoutModule,
    ReactiveFormsModule,
    ChartsModule,
    MatButtonModule,
	HttpClientModule
  ],
  declarations: [
    AppComponent,
    ManagementChartComponent,
    ManagementLogComponent,
    FPTBalanceComponent,
    CountTransactionComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
