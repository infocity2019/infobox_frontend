export interface IFormRegister {
  username: string;
  password: string;
  custCd?: string;
  typeUser?: string;
  systemDate?: string;
  email: string;
}
