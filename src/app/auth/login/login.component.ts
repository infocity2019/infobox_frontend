import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService, HttpError } from '../shared';
import * as _ from 'lodash';
import {DatePipe} from '@angular/common';
import {IFormRegister} from './interface/IFormRegister';
import {IFormGetPinCode} from './interface/IFormGetPinCode';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  ERROR_ENTER_USERNAME = 0;
  ERROR_ENTER_PASSWORD = 1;
  ERROR_ENTER_USERNAME_PASSWORD = 2;
  ERROR_SERVER = 3;
  ERROR_WRONG_USERNAME_PASSWORD = 4;
  ERROR_PASSWORD_SHORT = 7;
  ERROR_PASS_NOT_MATCH = 9;
  ERROR_ENTER_INFO = 8;
  ERROR_USER_NAME_EXIST = 10;
  ERROR_WRONG_EMAIL = 12;
  EMAIL_HAVE_SEND = 13;
  VALIDATE_CODE_WRONG = 14;
  LOGIN_OK = 5;
  onShowLogin: boolean;
  onShowChangePassword: boolean;
  errorLogin: number;
  btnRegisterShow: boolean;
  btnLoginShow: boolean;
  errorRegister: number;
  userNameLogin: any;
  userPasswordLogin: any;
  onShowValidateCode: boolean;
  userNameRegister: any;
  passWordRegister: any;
  confirmPassWordRegister: any;
  emailRegister: any;
  custCDRegister: any;
  typeUserRegister: any;
  startDateRegister: any;
  endDateRegister: any;
  mobileUserRegister: any;
  addressUserRegister: any;
  systemDateRegister: any;
  workIDRegister: any;
  REGISTER_SUCCESS = 11;
  validateCodeEmail: any;
  showTxtConfirmRegister: boolean;
  valueValidateCodeEmail: any;
  codeFromEmail: any;
  onShowConfirmRegister: boolean;
  inputValidateCode: boolean;
  errorEnterUsername: boolean;
  errorEnterCusCode: boolean;
  errorEnterTypeUser: boolean;
  errorEnterStartDate: boolean;
  errorEnterEndDate: boolean;
  errorEnterMobile: boolean;
  errorEnterAddress: boolean;
  errorEnterSystemDate: boolean;
  errorEnterWorkID: boolean;
  isCheckValid = false;
  mesValidate: string;
  messageLogin: string;
  isLogin: boolean;
  userId: any;


  loginForm = (new FormBuilder()).group({
    userID: ['', Validators.compose([Validators.required])],
    password: ['', Validators.required]
  });
  confirmForm = this.fb.group({
    validatecode: ['', [Validators.required]]
  });

  message = '';
  validateCodeMess: string;
  changePasswordForm = this.fb.group({
    password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    passwordconfirm: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
  }, {
    validator: this.MustMatch('password', 'passwordconfirm')
  });
  ResultAfterSendMessage: IFormGetPinCode;
  isSendPinCode: boolean;
  isChangePassword: boolean;
  registerMessage: string;
  registerMessageError: any;



  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private fb: FormBuilder,
    private datePipe: DatePipe
  ) {
  }


  ngOnInit() {
    localStorage.clear();
    this.onShowLogin = true;
    this.onShowChangePassword = false;
    this.onShowValidateCode = false;
    this.btnRegisterShow = true;
    this.showTxtConfirmRegister = false;

  }

  get f() {
    return this.changePasswordForm.controls;
  }

  get R() {
    return this.confirmForm.controls;
  }


  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }


  login() {
    this.errorLogin = 7;
    const redirect = '/default';
    const query = this.route.snapshot.queryParamMap;
    const userInput = this.loginForm.value;
    const data = {
      userID: userInput.userID,
      password: userInput.password,
    };

    if (_.isEmpty(userInput.userID) && !_.isEmpty(userInput.password)) {
      this.errorLogin = this.ERROR_ENTER_USERNAME;
    } else if (_.isEmpty(userInput.password) && !_.isEmpty(userInput.userID)) {
      this.errorLogin = this.ERROR_ENTER_PASSWORD;
    } else if (_.isEmpty(userInput.userID) && _.isEmpty(userInput.password)) {
      this.errorLogin = this.ERROR_ENTER_USERNAME_PASSWORD;
    } else {
      this.isLogin = true;
      this.userId = userInput.userID;
      this.authService.Login(data).subscribe(result => {
        this.isLogin = false;
        if (!result.status) {
          console.log(result);
          localStorage.setItem('Role', JSON.stringify(result.role));
          localStorage.setItem('currentUser', result.token);
          localStorage.setItem('UserName', result.user.USER_ID);
          localStorage.setItem('NameUser', result.user.USER_NM);
          localStorage.setItem('loginTime', Date.now().toString());
          localStorage.setItem('currentUserJson', JSON.stringify(result.user));
          if (result.user.userid != null) {
            this.errorLogin = this.LOGIN_OK;
            this.router.navigateByUrl('/default');
          } else {
            console.log('Not Ok');
            this.router.navigateByUrl('');
          }
        } else if (result.status == 202) {
          this.onShowLogin = false;
          this.onShowChangePassword = true;
          localStorage.setItem('currentUser', result.token);
          console.log(result.status);
          console.log('need change password');
        }
      }, (e) => {
        this.isLogin = false;
        if (e.status === 0) {
          this.errorLogin = this.ERROR_SERVER;
        } else if(e.status == 403) {
          this.messageLogin = e.message;
        }else {
          this.errorLogin = this.ERROR_WRONG_USERNAME_PASSWORD;
        }
      });
    }
  }

  changePasswordFirstTimeLogin(openNotifyChangPassword: HTMLButtonElement) {
    const inputData = this.changePasswordForm.value;
    if (this.changePasswordForm.invalid) {
      return;
    }
    if (inputData.password == 'nice1234') {
      return this.mesValidate = 'This password invalid! Please try other password.';
    }
    this.isChangePassword = true;
    const data = {
      userID: this.userId,
      password: inputData.password,
    };
    this.authService.changePassword(data).subscribe(
      result => {
        this.isChangePassword = false;
        if (result.rowsAffected == 1) {
          openNotifyChangPassword.click();
        }
      }, error => {
        this.mesValidate = error.message;
        console.log(error);
      }
    );
  }

  showRegister() {
    this.registerMessage = '';
    this.onShowChangePassword = true;
    this.onShowLogin = false;
    this.onShowConfirmRegister = false;
    this.btnRegisterShow = false;
    this.btnLoginShow = true;
    this.userNameRegister = '';
    this.passWordRegister = '';
    this.confirmPassWordRegister = '';
    this.emailRegister = '';
    this.errorRegister = null;
  }

  showLogin(closeModalNotifyChangePassword: HTMLButtonElement) {
    this.mesValidate = '';
    this.userNameLogin = '';
    this.userPasswordLogin = '';
    this.errorLogin = null;
    this.onShowConfirmRegister = false;
    this.onShowChangePassword = false;
    this.onShowLogin = true;
    this.btnRegisterShow = true;
    this.btnLoginShow = false;
    closeModalNotifyChangePassword.click();
  }
}
