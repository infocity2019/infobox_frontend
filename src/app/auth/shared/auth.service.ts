import { Injectable } from "@angular/core";
import { IUser, HttpServiceGenerator } from './shared';
import { tap, map } from "rxjs/operators";
import {Observable} from "rxjs";
import {IFormRegister} from '../login/interface/IFormRegister';
import {IFormGetPinCode} from '../login/interface/IFormGetPinCode';

interface IAuthOption {
    // scope: string;
    // branch_code: string;
    userID: string;
    password: string;
    // auto_login: boolean;
}

interface OptionCheckE {
    username: string;
    email: string;
}

interface Result {
    username: string;
    password: string;
    custCd: string;
    typeUser: string;
    systemDate: string;
    email: string;
}

interface ISession {
    id?: string;
}

interface ILoginReply {
    token: ISession;
    user: IUser;
}

@Injectable()
export class AuthService {
  private userAPI = this.httpG.make('/user');
  private authApi = this.httpG.make("/auth");
    constructor(
        private httpG: HttpServiceGenerator
    ) { }

    Login(option: IAuthOption): Observable<any> {
        return this.authApi.Post<any>("login", {}, option).pipe(tap(data => {
          console.log(data);
        }));;
    }

    changePassword(data: IAuthOption): Observable<any> {
      return this.userAPI.Put<any>('changePassword', {} , data);
    }

    SendEmail(option: IFormRegister): Observable<IFormGetPinCode> {
        return this.authApi.Post<IFormGetPinCode>('sendEmail', {}, option);
    }

    CheckEmail(optionCheckE: OptionCheckE) {
        return this.authApi.Post('checkemail', {}, optionCheckE)
            .pipe(tap(data => {
                console.log('checkemail', data);
            }));
    }

    GetCodeEmail(option: IAuthOption) {
        return this.authApi.Post('getCodeEmail', {}, option)
            .pipe(tap(data => {
                console.log('getCodeEmail', data);
            }));
    }

    Register(res: IFormRegister): Observable<any> {
        return this.authApi.Post<any>('register', {}, res)
            .pipe(tap(data => {
                console.log('Register success', data);
            }));
    }

    ValidateSession(scope?: string, token?: string) {
        console.log("current user~~~~", localStorage.getItem('currentUser'));
        return Object.keys(localStorage.getItem('currentUser')).length === 0;
    }


}
