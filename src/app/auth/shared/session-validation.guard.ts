import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class SessionValidationGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | boolean {
        if (localStorage.getItem('currentUser') == null) {
            this.ShowLogin(state.url);
            return false;
        } else {
          return true;
        }
    }
    private ShowLogin(redirect: string) {
        this.router.navigate(["/auth/login"]);
    }
}
