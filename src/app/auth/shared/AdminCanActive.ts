import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import {CheckRoleService} from '../../shared/service/check-role.service';

@Injectable()
export class AdminCanActive implements CanActivate {
  private checkRoleAccount: { isAccessCaptcha: boolean; isRoleAdmin: boolean; isRoleUser: boolean };
  constructor(
    private router: Router,
    private authService: AuthService,
    private checkRole: CheckRoleService
  ) {
    this.checkRoleAccount = this.checkRole.checkRoleAccount();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (!this.checkRoleAccount.isRoleAdmin) {
      return false;
    } else {
      return true;
    }
  }

}
